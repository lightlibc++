/*
Permission is granted to use, modify, and / or redistribute at will.

This includes removing authorship notices, re-use of code parts in
other software (with or without giving credit), and / or creating a
commercial product based on it.

This permission is not revocable by the author.

This software is provided as-is. Use it at your own risk. There is
no warranty whatsoever, neither expressed nor implied, and by using
this software you accept that the author(s) shall not be held liable
for any loss of data, loss of service, or other damages, be they
incidental or consequential. Your only option other than accepting
this is not to use the software at all.
*/
#include <exception>
#include <cstdlib>           // std::abort()



/*
 * file-local functions
 */
static void default_unexpected_handler()
{
  std::terminate();
}

static void default_terminate_handler()
{
  std::abort();
}



/*
 * file-local variables
 */
static std::unexpected_handler cur_unexpected_handler = &default_unexpected_handler;
static std::terminate_handler cur_terminate_handler = &default_terminate_handler;



#ifndef _LIGHTLIBCPP_NO_EXCEPTIONS

  /*
  * Class std::exception
  */

  std::exception::~exception() throw()
  {
  }

  const char* std::exception::what() const throw()
  {
    return "std::exception";
  }



  /*
  * Class std::bad_exception
  */

  const char* std::bad_exception::what() const throw()
  {
    return "std::bad_exception";
  }

  std::bad_exception::~bad_exception() throw()
  {
  }

#endif



/*
 * functions
 */

std::unexpected_handler std::set_unexpected(unexpected_handler f) throw()
{
  unexpected_handler tmp = cur_unexpected_handler;
  cur_unexpected_handler = f;
  return tmp;
}

std::unexpected_handler std::__get_unexpected()
{
  return cur_unexpected_handler;
}

void std::unexpected()
{
  cur_unexpected_handler();
}

std::terminate_handler std::set_terminate(terminate_handler f) throw()
{
  terminate_handler tmp = cur_terminate_handler;
  cur_terminate_handler = f;
  return tmp;
}

std::terminate_handler std::__get_terminate()
{
  return cur_terminate_handler;
}

void std::terminate()
{
  cur_terminate_handler();
}
