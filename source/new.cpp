/*
Permission is granted to use, modify, and / or redistribute at will.

This includes removing authorship notices, re-use of code parts in
other software (with or without giving credit), and / or creating a
commercial product based on it.

This permission is not revocable by the author.

This software is provided as-is. Use it at your own risk. There is
no warranty whatsoever, neither expressed nor implied, and by using
this software you accept that the author(s) shall not be held liable
for any loss of data, loss of service, or other damages, be they
incidental or consequential. Your only option other than accepting
this is not to use the software at all.
*/
#include <new>
#include <cstdlib>           // malloc, free



/*
 * global variables
 */
const std::nothrow_t std::nothrow = std::nothrow_t();



/*
 * file-local variables
 */
static std::new_handler cur_new_handler = 0;



/*
 * Class std::bad_alloc
 */

#ifndef _LIGHTLIBCPP_NO_EXCEPTIONS

  std::bad_alloc::~bad_alloc() throw()
  {
  }

  const char* std::bad_alloc::what() const throw()
  {
    return "std::bad_alloc";
  }

#endif



/*
 * functions
 */

void* operator new(std::size_t size)
#ifndef _LIGHTLIBCPP_NO_EXCEPTIONS
  throw(std::bad_alloc)
#else
  throw()
#endif
{
  while (true)
  {
    // Try to allocate the space
    void* p = std::malloc(size);

    // Return a pointer to the allocated space,
    // if the allocation succeeded
    if (p != 0)
      return p;

    // Throw std::bad_alloc if there is no new_handler installed
    if (cur_new_handler == 0)
    #ifndef _LIGHTLIBCPP_NO_EXCEPTIONS
      throw std::bad_alloc();
    #else
      return 0;
    #endif

    // Call the current new_handler
    cur_new_handler();
  }
}

void* operator new(std::size_t size,
                   const std::nothrow_t&) throw()
{
  while (true)
  {
    // Try to allocate the space
    void* p = std::malloc(size);

    // Return a pointer to the allocated space,
    // if the allocation succeeded
    if (p != 0)
      return p;

    // Throw std::bad_alloc if there is no new_handler installed
    if (cur_new_handler == 0)
      return 0;

    // Call the current new_handler and return 0 if the new_handler
    // throws a bad_alloc exception
    #ifndef _LIGHTLIBCPP_NO_EXCEPTIONS
      try
      {
    #endif

        cur_new_handler();

    #ifndef _LIGHTLIBCPP_NO_EXCEPTIONS
      }
      catch (const std::bad_alloc& e)
      {
        return 0;
      }
    #endif
  }
}

void operator delete(void* ptr) throw()
{
  // Deallocate the space
  std::free(ptr);
}

void operator delete(void* ptr,
                     const std::nothrow_t&) throw()
{
  // Deallocate the space
  std::free(ptr);
}

void* operator new[](std::size_t size)
#ifndef _LIGHTLIBCPP_NO_EXCEPTIONS
  throw(std::bad_alloc)
#else
  throw()
#endif
{
  // Allocate via the non-array form of new
  return operator new(size);
}

void* operator new[](std::size_t size,
                     const std::nothrow_t&) throw()
{
  // Allocate via the non-array form of nothrow-new
  return operator new (size, std::nothrow);
}

void operator delete[](void* ptr) throw()
{
  // Deallocate the space
  std::free(ptr);
}

void operator delete[](void* ptr,
                       const std::nothrow_t&) throw()
{
  // Deallocate the space
  std::free(ptr);
}

void* operator new(std::size_t,
                   void* ptr) throw()
{
  return ptr;
}

void* operator new[](std::size_t,
                     void* ptr) throw()
{
  return ptr;
}

void operator delete(void*,
                     void*) throw()
{
}

void operator delete[](void*,
                       void*) throw()
{
}

std::new_handler std::set_new_handler(new_handler new_p) throw()
{
  std::new_handler old_new_handler = cur_new_handler;
  cur_new_handler = new_p;
  return old_new_handler;
}
