/*
Permission is granted to use, modify, and / or redistribute at will.

This includes removing authorship notices, re-use of code parts in
other software (with or without giving credit), and / or creating a
commercial product based on it.

This permission is not revocable by the author.

This software is provided as-is. Use it at your own risk. There is
no warranty whatsoever, neither expressed nor implied, and by using
this software you accept that the author(s) shall not be held liable
for any loss of data, loss of service, or other damages, be they
incidental or consequential. Your only option other than accepting
this is not to use the software at all.
*/
#include <typeinfo>
#include <cxxabi.h>
#include <string.h>
// TODO #include <cstring>           // std::strcmp



#ifndef _LIGHTLIBCPP_NO_RTTI



/*
 * class std::type_info
 */

bool std::type_info::before(const type_info& x) const
{
  // TODO std::strcmp
  return (strcmp(__type_name, x.__type_name) < 0);
}



/*
 * class abi::__fundamental_type_info
 * NOTE: Emits the __fundamental_type_info's for the following types:
 *       void, bool, wchar_t, char, unsigned char, signed char, short,
 *       unsigned short, int, unsigned int, long, unsigned long, long long,
 *       unsigned long long, float, double, long double and for every pointer
 *       and pointer-to-const to these types
 *       And Emits vtables and typeinfo for __fundamental_type_info
 */

abi::__fundamental_type_info::~__fundamental_type_info()
{
}



/*
 * NOTE: Emits vtables and typeinfo for __array_type_info, __function_type_info,
 *       __enum_type_info, __pointer_to_member_type_info, __class_type_info,
 *       __si_class_type_info and __vmi_class_type_info
 */

abi::__array_type_info::~__array_type_info()
{
}

abi::__function_type_info::~__function_type_info()
{
}

abi::__enum_type_info::~__enum_type_info()
{
}

abi::__pbase_type_info::~__pbase_type_info()
{
}

abi::__pointer_type_info::~__pointer_type_info()
{
}

abi::__pointer_to_member_type_info::~__pointer_to_member_type_info()
{
}

abi::__class_type_info::~__class_type_info()
{
}

abi::__si_class_type_info::~__si_class_type_info()
{
}

abi::__vmi_class_type_info::~__vmi_class_type_info()
{
}



#endif
