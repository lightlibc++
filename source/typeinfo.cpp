/*
Permission is granted to use, modify, and / or redistribute at will.

This includes removing authorship notices, re-use of code parts in
other software (with or without giving credit), and / or creating a
commercial product based on it.

This permission is not revocable by the author.

This software is provided as-is. Use it at your own risk. There is
no warranty whatsoever, neither expressed nor implied, and by using
this software you accept that the author(s) shall not be held liable
for any loss of data, loss of service, or other damages, be they
incidental or consequential. Your only option other than accepting
this is not to use the software at all.
*/
#include <typeinfo>


#ifndef _LIGHTLIBCPP_NO_RTTI

  /*
  * class std::type_info
  */

  std::type_info::~type_info()
  {
  }

#endif



#ifndef _LIGHTLIBCPP_NO_EXCEPTIONS

  /*
  * class std::bad_cast
  */

  std::bad_cast::~bad_cast() throw()
  {
  }

  const char* std::bad_cast::what() const throw()
  {
    return "std::bad_cast";
  }



  /*
  * class std::bad_typeid
  */

  std::bad_typeid::~bad_typeid() throw()
  {
  }

  const char* std::bad_typeid::what() const throw()
  {
    return "std::bad_typeid";
  }

#endif
