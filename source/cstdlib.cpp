/*
Permission is granted to use, modify, and / or redistribute at will.

This includes removing authorship notices, re-use of code parts in
other software (with or without giving credit), and / or creating a
commercial product based on it.

This permission is not revocable by the author.

This software is provided as-is. Use it at your own risk. There is
no warranty whatsoever, neither expressed nor implied, and by using
this software you accept that the author(s) shall not be held liable
for any loss of data, loss of service, or other damages, be they
incidental or consequential. Your only option other than accepting
this is not to use the software at all.
*/
#ifndef _LIGHTLIBCPP_CPP03_STRICT



#include <cstdlib>
#include <cstddef>           // std::size_t
#include <exception>         // std::terminate



/*
 * defines
 */
#define _LIGHTLIBCPP_QUICK_EXIT_ARRAY_SIZE       32



/*
 * types
 */
typedef void (*quick_exit_function_t)(void);



/*
 * file-local variables
 */
static bool quick_exit_in_progress = false;
static quick_exit_function_t quick_exit_functions[_LIGHTLIBCPP_QUICK_EXIT_ARRAY_SIZE];
static std::size_t quick_exit_functions_index = 0;



/*
 * functions
 */

int std::at_quick_exit(quick_exit_function_t f)
{
  // TODO: Ensure thread-safety

  // Are currently in quick_exit?
  if (quick_exit_in_progress)
  {
    // Call the function directly now
    f();
  }
  else
  {
    // Do we have enough space in the array
    if (quick_exit_functions_index == _LIGHTLIBCPP_QUICK_EXIT_ARRAY_SIZE)
      return -1;

    // Save the function pointer
    quick_exit_functions[quick_exit_functions_index++] = f;
  }

  return 0;
}

void std::quick_exit(int status)
{
  // TODO: Ensure thread-safety

  quick_exit_in_progress = true;

  for (;quick_exit_functions_index > 0;--quick_exit_functions_index)
  {
    #ifndef _LIGHTLIBCPP_NO_EXCEPTIONS
      try
      {
    #endif

        quick_exit_functions[quick_exit_functions_index - 1]();

    #ifndef _LIGHTLIBCPP_NO_EXCEPTIONS
      }
      catch (...)
      {
        std::terminate();
      }
    #endif
  }

  std::_Exit(status);
}



#endif
