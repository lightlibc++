         lightlibc++
         ===========

Contents:
1. What is this?
2. What requirements?
3. How do I use it?
4. How do I test it?


1. What is this?
================

This is a C++ standard library, that is supposed to support the ISO/IEC C++
ISO/IEC 14882:2003 and the ISO/IEC C++ [TODO: 1x] standards. It is written to
be portable. It is supposed to be especially usable in hobby operating systems
environments.

2. What requirements
====================

A C library is required and the in libc.README documentated changes have to be
made.

3. How do I use it?
===================

lightlibc++ comes with a Makefile for a gcc-based host-environment, so that you
can compile the code and run the testsuite for the library on your host
platform. If you choose to use lightlibc++ in your own hobby operating system
make sure that you have a working libc. Apart from that the only thing you have
to do is write a Makefile, that builds the library for your operating system.
You might want to have a look at the Makefile provided for compilation and
testing on the host operating system.
Apart from that you can use the following defines to configure the library:

* _LIGHTLIBCPP_CPP03
  The library uses only C++03 language features and provides everything it can
  with these features (including C++10 library stuff)
* _LIGHTLIBCPP_CPP03_STRICT
  The library uses only C++03 language features and provides only the C++03
  standard library stuff
* _LIGHTLIBCPP_CPP10
  The library uses C++10 language features and provides the full C++03 and
  C++10 library
* _LIGHTLIBCPP_CPP10_STRICT
  The library uses C++10 language features and provides the full C++10 library
  and marks deprecated stuff from C++03 accordingly
* _LIGHTLIBCPP_CPP10_NO_RVALUE_REFERENCES
  The library does not use rvalue references
* _LIGHTLIBCPP_CPP10_NO_VARIADIC_TEMPLATES
  The library does not use variadic templates
* _LIGHTLIBCPP_CPP10_DEFAULT_DELETE_FUNCTIONS
  The library does not use defaulted/deleted functions
* _LIGHTLIBCPP_NO_EXCEPTIONS
  The library does not use exceptions
* _LIGHTLIBCPP_NO_RTTI
  The library does not use rtti

The
* _LIGHTLIBCPP_CPP03 and _LIGHTLIBCPP_CPP10
* _LIGHTLIBCPP_NO_EXCEPTIONS
* _LIGHTLIBCPP_NO_RTTI
defines are automatically defined in lightlibc++/compiler.hpp for g++ according
to the -fno-exceptions, -fno-rtti (gcc 4.3 or later only), -std=c++98 and
-std=c++0x compiler switches.

The
* _LIGHTLIBCPP_CPP10_NO_VARIADIC_TEMPLATES
* _LIGHTLIBCPP_CPP10_NO_DEFAULT_DELETE_FUNCTIONS
* _LIGHTLIBCPP_CPP10_NO_RVALUE_REFERENCES
defines are automatically defined in lightlibc++/compiler.hpp for g++ according
to the gcc version.


4. How do I test it?
====================

The supplied Makefile comes with an 'all-host' target which will build the
library on the host platform (gcc required), an 'testcases-host' target which
will build and execute the testcases on the host platform.
