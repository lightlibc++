/*
Permission is granted to use, modify, and / or redistribute at will.

This includes removing authorship notices, re-use of code parts in
other software (with or without giving credit), and / or creating a
commercial product based on it.

This permission is not revocable by the author.

This software is provided as-is. Use it at your own risk. There is
no warranty whatsoever, neither expressed nor implied, and by using
this software you accept that the author(s) shall not be held liable
for any loss of data, loss of service, or other damages, be they
incidental or consequential. Your only option other than accepting
this is not to use the software at all.
*/
#ifndef _LIGHTLIBCPP_TYPEINFO_HPP
#define _LIGHTLIBCPP_TYPEINFO_HPP



#include <lightlibc++/compiler.hpp>
#include <exception>                   // std::exception



namespace std
{
  /** \addtogroup lightlibcpp_18_7 */
  /*@{*/

#ifndef _LIGHTLIBCPP_NO_RTTI

  /**
   *\english
   *  \brief Base class for runtime type-information return by the typeid
   *         operator
   *\endenglish
   *\german
   *  \brief Basisklasse für Laufzeittypinformationen vom typeid-Operator
   *\endgerman
   */
  class type_info
  {
    public:
      /**
       *\english
       *  analogue to the == operator
       *  \param[in] x the object to compare to
       *\endenglish
       *\german
       *  analog dem == Operator
       *  \param[in] x das Objekt mit dem verglichen werden soll
       *\endgerman
       */
      inline bool operator != (const type_info& x) const
      {
        return !(*this == x);
      }

      /**
       *\english
       *  mangled name of the type the type_info references
       *  \note The described functionality is specific to the implemented C++
       *        ABI
       *\endenglish
       *\german
       *  'mangled name' des Typs den diese type_info Klasse beschreibt
       *  \note Die beschriebene Funktionalität ist abhängig von der
       *        implementierten C++ ABI
       *\endgerman
       */
      inline const char* name() const
      {
        return __type_name;
      }

      /**
       *\english
       *  \note The implementation of this function is part of the C++ ABI
       *  \param[in] x the object to compare to
       *  \return true, if two type_info's reference the same type, false
       *          otherwise
       *\endenglish
       *\german
       *  \note Die Implementation dieser Funktion ist Teil der C++ ABI
       *  \param[in] x das Objekt mit dem verglichen werden soll
       *  \return true, falls zwei type_info's den gleichen Typ beschreiben,
       *          sonst false
       *\endgerman
       */
      inline bool operator == (const type_info& x) const
      {
        return (__type_name == x.__type_name);
      }

      /** \todo what is this supposed to be used for?
       *  \param x */
      bool before(const type_info& x) const;

      // TODO C++1x: size_t hash_code() const throw()

      /**
       *\english
       *  Destructor
       *\endenglish
       *\german
       *  Destruktor
       *\endgerman
       */
      virtual ~type_info();

  #ifndef _LIGHTLIBCPP_CPP10_NO_DEFAULT_DELETE_FUNCTIONS

      /**
       *\english
       *  \note No copy-constructor available
       *\endenglish
       *\german
       *  \note Kein Kopierkonstruktor verfügbar
       *\endgerman
       */
      type_info(const type_info&) = delete;

      /**
       *\english
       *  \note No assignment-operator available
       *\german
       *  \note Kein Zuweisungsoperator verfügbar
       *\endgerman
       */
      type_info& operator = (const type_info&) = delete;

  #else

    private:
      /**
       *\english
       *  \note No copy-constructor available
       *\endenglish
       *\german
       *  \note Kein Kopierkonstruktor verfügbar
       *\endgerman
       */
      type_info(const type_info&);

      /**
       *\english
       *  \note No assignment-operator available
       *\endenglish
       *\german
       *  \note Kein Zuweisungsoperator verfügbar
       *\endgerman
       */
      type_info& operator = (const type_info&);

  #endif

    private:
    #if defined(__GNUC__) && \
        defined(__GXX_ABI_VERSION) && \
        __GXX_ABI_VERSION >= 100
      /**
       *\english
       *  pointer to the mangled name of the type
       *  \warning This member is part of the C++ ABI
       *\endenglish
       *\german
       *  Zeiger auf den 'mangled name' des Typs
       *  \warning Dieser Member ist Teil der C++ ABI
       *\endgerman
       */
      const char* __type_name;
    #else
      #error std::type_info for your compiler or compiler version is not supported
    #endif
  };

#endif



  // TODO C++1x: class type_index
  // TODO C++1x: template<class T>struct hash
  // TODO C++1x: template<>struct hash<type_index>



#ifndef _LIGHTLIBCPP_NO_EXCEPTIONS

  /**
   *\english
   *  \brief Class thrown by an invalid dynamic_cast expression
   *\endenglish
   *\german
   *  \brief Klasse die von einem ungültigen dynamic_cast Ausdruck geworfen
   *         wird
   *\endgerman
   */
  class bad_cast : public std::exception
  {
    public:
      /**
       *\english
       *  Constructs an object of class bad_cast.
       *\endenglish
       *\german
       *  Konstruiert ein Objekt der Klasse bad_cast.
       *\endgerman
       */
      inline bad_cast() throw()
      {
      }

      /**
       *\english
       *  Copies an object of class bad_exception.
       *  \param[in] x reference to the object to copy
       *\endenglish
       *\german
       *  Kopiert ein Objekt der Klasse bad_exception.
       *  \param[in] x Referenz auf das zu kopierende Objekt
       *\endgerman
       */
      inline bad_cast(const bad_cast& x) throw()
        : std::exception(x)
      {
      }

      /**
       *\english
       *  Destructs an object of class bad_cast
       *\endenglish
       *\german
       *  Zerstört ein Objekt der Klasse bad_cast
       *\endgerman
       */
      virtual ~bad_cast() throw();

      virtual const char* what() const throw();
  };

  /**
   *\english
   *  \brief Class thrown by the implementation to report a null pointer in a
   *         typeid expression
   *\endenglish
   *\german
   *  \brief Klasse die von der Implementierung geworfen wird, um einen
   *         Nullzeiger in einem typeid Ausdruck zu melden
   *\endgerman
   */
  class bad_typeid : public std::exception
  {
    public:
      /**
       *\english
       *  Constructs an object of class bad_typeid.
       *\endenglish
       *\german
       *  Konstruiert ein Objekt der Klasse bad_typeid.
       *\endgerman
       */
      inline bad_typeid() throw()
      {
      }

      /**
       *\english
       *  Copies an object of class bad_typeid.
       *  \param[in] x reference to the object to copy
       *\endenglish
       *\german
       *  Kopiert ein Objekt der Klasse bad_typeid.
       *  \param[in] x Referenz auf das zu kopierende Objekt
       *\endgerman
       */
      inline bad_typeid(const bad_typeid& x) throw()
        : std::exception(x)
      {
      }

      /**
       *\english
       *  Destructs an object of class bad_typeid
       *\endenglish
       *\german
       *  Zerstört ein Objekt der Klasse bad_typeid
       *\endgerman
       */
      virtual ~bad_typeid() throw();

      virtual const char* what() const throw();
  };

#endif



  /*@}*/
}



#endif
