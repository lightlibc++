/*
Permission is granted to use, modify, and / or redistribute at will.

This includes removing authorship notices, re-use of code parts in
other software (with or without giving credit), and / or creating a
commercial product based on it.

This permission is not revocable by the author.

This software is provided as-is. Use it at your own risk. There is
no warranty whatsoever, neither expressed nor implied, and by using
this software you accept that the author(s) shall not be held liable
for any loss of data, loss of service, or other damages, be they
incidental or consequential. Your only option other than accepting
this is not to use the software at all.
*/
#ifndef _LIGHTLIBCPP_INITIALIZER_LIST_HPP
#define _LIGHTLIBCPP_INITIALIZER_LIST_HPP



#ifdef _LIGHTLIBCPP_CPP03_STRICT
  #error The <initializer_list> Header is not available in strict ISO/IEC 14882:2003 mode
#endif



#include <lightlibc++/compiler.hpp>
#include <cstddef>                     // std::size_t



namespace std
{
  /** \addtogroup lightlibcpp_18_9 */
  /*@{*/

  /**
   *\english
   *  \brief class template for initializer lists
   *
   *  \param E type of the underlying objects of the list
   *\endenglish
   *\german
   *  \brief Klassenschablone für initializer lists
   *
   *  \param E Typ der Objekte der Liste
   *\endgerman
   */
  template<typename E>
  class initializer_list
  {
    public:
      /** Type of the initializer list's elements */
      typedef E           value_type;
      /** Reference to value_type */
      typedef const E&    reference;
      /** Constant reference to value_type */
      typedef const E&    const_reference;
      /** Type used to describe the initiailizer list's size */
      typedef std::size_t size_type;
      /** Iterator */
      typedef const E*    iterator;
      /** Constant iterator */
      typedef const E*    const_iterator;



      /**
       *\english
       *  Default constructor constructs an empty initializer list
       *\endenglish
       *\german
       *  Der Standardkonstruktor erstellt eine leere Initializer List
       *\endgerman
       */
      inline initializer_list()
        : m_begin(0), m_size(0)
      {
      }

    #if defined(__GNUC__)

      inline initializer_list(const_iterator begin,
                              size_type size)
        : m_begin(begin), m_size(size)
      {
      }

    #endif

      /**
       *\english
       *  \return the number of elements in the initializer list
       *\endenglish
       *\german
       *  \return die Anzahl der Elemente in der Initializer List
       *\endgerman
       */
      inline size_type size() const
      {
        return m_size;
      }

      /**
       *\english
       *  \return an constant iterator to the beginning of the initializer list
       *\endenglish
       *\german
       *  \return ein konstanter Iterator, der auf den Anfang der Initializer
       *          List zeigt
       *\endgerman
       */
      inline const_iterator begin() const
      {
        return m_begin;
      }

      /**
       *\english
       *  \return an constant iterator to the end of the initializer list
       *\endenglish
       *\german
       *  \return ein konstanter Iterator, der auf das Ende der Initializer
       *          List zeigt
       *\endgerman
       */
      inline const_iterator end() const
      {
        return m_begin + m_size;
      }

    private:
    #if defined(__GNUC__)
      const_iterator m_begin;
      size_type      m_size;
    #else
      #error Your compiler or compiler version is not supported
    #endif
  };

  /*@}*/
}



#endif
