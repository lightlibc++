/*
Permission is granted to use, modify, and / or redistribute at will.

This includes removing authorship notices, re-use of code parts in
other software (with or without giving credit), and / or creating a
commercial product based on it.

This permission is not revocable by the author.

This software is provided as-is. Use it at your own risk. There is
no warranty whatsoever, neither expressed nor implied, and by using
this software you accept that the author(s) shall not be held liable
for any loss of data, loss of service, or other damages, be they
incidental or consequential. Your only option other than accepting
this is not to use the software at all.
*/
#ifndef _LIGHTLIBCPP_EXCEPTION_HPP
#define _LIGHTLIBCPP_EXCEPTION_HPP



#include <lightlibc++/compiler.hpp>



namespace std
{
  /** \addtogroup lightlibcpp_18_8 */
  /*@{*/

#ifndef _LIGHTLIBCPP_NO_EXCEPTIONS

  /**
   *\english
   *  \brief Base class for exceptions thrown by the C++ Standard Library
   *
   *  18.8.1 Class exception
   *
   *  Base class for exceptions thrown by the C++ Standard Library
   *\endenglish
   *\german
   *  \brief Basisklasse der Ausnahmen, die von der C++ Standardbibliothek geworfen werden
   *
   *  18.8.1 Klasse exception
   *
   *  Basisklasse der Ausnahmen, die von der C++ Standardbibliothek geworfen werden
   *\endgerman
   */
  class exception
  {
    public:
      /**
       *\english
       *  Constructs an object of class exception.
       *\endenglish
       *\german
       *  Konstruiert ein Objekt der Klasse exception.
       *\endgerman */
      inline exception() throw()
      {
      }

      /**
       *\english
       *  Copies an object of class exception.
       *  \param[in] x reference to the object to copy
       *\endenglish
       *\german
       *  Kopiert ein Objekt der Klasse exception.
       *  \param[in] x Referenz auf das zu kopierende Objekt
       *\endgerman */
      inline exception(const exception& x) throw()
      {
        (void)x;
      }

      /**
       *\english
       *  Copies an object of class exception.
       *  \param[in] x reference to the object to copy
       *\endenglish
       *\german
       *  Kopiert ein Objekt der Klasse exception.
       *  \param[in] x Referenz auf das zu kopierende Objekt
       *\endgerman */
      inline exception& operator = (const exception& x) throw()
      {
        (void)x;
        return *this;
      }

      /**
       *\english
       *  Destructs an object of class exception
       *\endenglish
       *\german
       *  Zerstört ein Objekt der Klasse exception
       *\endgerman */
      virtual ~exception() throw();

      /**
       *\english
       *  User-friendly name of the class
       *  \return null-terminated character sequence
       *\endenglish
       *\german
       *  Benutzerfreundlicher Name der Klasse
       *  \return nullterminierte Zeichenkette
       *\endgerman
       */
      virtual const char* what() const throw();
  };

#endif

  /*@}*/



  /** \addtogroup lightlibcpp_18_8_2 */
  /*@{*/

#ifndef _LIGHTLIBCPP_NO_EXCEPTIONS

  /**
   *\english
   *  \brief Class thrown by the runtime, when an exception-specification is
   *         violated
   *
   *  18.8.2.1 Class bad_exception
   *
   *  Class thrown by the runtime, when an exception-specification is violated
   *\endenglish
   *\german
   *  \brief Klasse die von der Laufzeit geworfen wird, wenn eine
   *         Ausnahmenspezifikation verletzt wurde
   *
   *  18.8.2.1 Klasse bad_exception
   *
   *  Klasse die von der Laufzeit geworfen wird, wenn eine
   *  Ausnahmenspezifikation verletzt wurde
   *\endgerman
   */
  class bad_exception : public std::exception
  {
    public:
      /**
       *\english
       *  Constructs an object of class bad_exception.
       *\endenglish
       *\german
       *  Konstruiert ein Objekt der Klasse bad_exception.
       *\endgerman */
      inline bad_exception() throw()
      {
      }

      /**
       *\english
       *  Copies an object of class bad_exception.
       *  \param[in] x reference to the object to copy
       *\endenglish
       *\german
       *  Kopiert ein Objekt der Klasse bad_exception.
       *  \param[in] x Referenz auf das zu kopierende Objekt
       *\endgerman */
      inline bad_exception(const bad_exception& x) throw()
        : std::exception(x)
      {
      }

      /**
       *\english
       *  Destructs an object of class bad_exception
       *\endenglish
       *\german
       *  Zerstört ein Objekt der Klasse bad_exception
       *\endgerman */
      virtual ~bad_exception() throw();

      virtual const char* what() const throw();
  };

#endif

  /**
   *\english
   *  18.8.2.2 Type unexpected_handler
   *
   *  The type of a handler function to be called by unexpected() when a
   *  function attempts to throw an exception not listed in its
   *  exception-speciﬁcation.
   *
   *  An unexpected_handler shall not return. 
   *
   *  The default unexpected_handler calls terminate().
   *\endenglish
   *\german
   *  18.8.2.2 Typ unexpected_handler
   *
   *  Der Typ einer Handlerfunktion, welche von unexpected() aufgerufen wird,
   *  wenn eine Funktion eine Ausnahme, die nicht in ihrer
   *  Ausnahmenspezifikation angegeben ist, wirft.
   *
   *  Ein unexpected_handler darf die Kontrolle nicht an die aufrufende
   *  Funktion zurückgeben.
   *
   *  Der standardmäßig installierte unexpected_handler ruft terminate() auf.
   *\endgerman
   */
  typedef void (*unexpected_handler)() _LIGHTLIBCPP_NORETURN;

  /**
   *  18.8.2.3 set_unexpected
   *
   *\english
   *  The unexpected_handler f becomes the new handler called by unexpected()
   *  \param[in] f a non-null pointer to the new unexpected_handler
   *  \return a pointer to the previous unexpected_handler
   *\endenglish
   *\german
   *  Der unexpected_handler f wird der neue Handler, der von unexpected()
   *  aufgerufen wird
   *  \param[in] f ein gültiger Zeiger auf einen unexpected_handler
   *  \return ein Zeiger auf den vorhergehende unexpected_handler
   *\endgerman
   */
  unexpected_handler set_unexpected(unexpected_handler f) throw();

  /**
   *\english
   *  lightlibc++ extension
   *
   *  Get the current unexpected_handler
   *  \note This function is needed by the C++ exception handling ABI
   *  \return apointer to the current unexpected_handler
   *\endenglish
   *\german
   *  lightlibc++ Erweiterung
   *
   *  Gibt einen Zeiger auf momentan installierten unexpected_handler zurück
   *  \note Diese Funktion wird von der C++ Ausnahmebehandlungs-ABI benötigt
   *  \return ein Zeiger auf den momentan installierten unexpected_handler
   *\endgerman
   */
  unexpected_handler __get_unexpected();

  /**
   *  18.8.2.4 unexpected
   *
   *\english
   *  Called by the implementation when a function exits via an exception not
   *  allowed by its exception-speciﬁcation. May also be called directly by the
   *  program.
   *
   *  Calls the calls the current unexpected_handler.
   *\endenglish
   *\german
   *  Diese Funktion wird vom der Laufzeit aufgerufen, falls eine Funktion eine
   *  Ausnahme, die nicht in ihrer Ausnahmenspezifikation angegeben ist, wirft.
   *  Diese Funktion darf auch direkt aus dem Programm aufgerufen werden.
   *
   *  unexpected() ruft den momentanen unexpected_handler auf.
   *\endgerman
   */
  void unexpected() _LIGHTLIBCPP_NORETURN;

  /*@}*/



  /** \addtogroup lightlibcpp_18_8_3 */
  /*@{*/

  /**
   *\english
   *  18.8.3.1 Type terminate_handler
   *
   *  The type of a handler function to be called by terminate() when
   *  terminating exception processing.
   *
   *  A terminate_handler shall terminate execution of the program without
   *  returning to the caller.
   *
   *  The default terminate_handler calls abort().
   *\endenglish
   *\german
   *  18.8.3.1 Typ terminate_handler
   *
   *  Der Typ einer Handlerfunktion, welche von terminate() aufgerufen wird,
   *  wenn die Ausnahmebehandlung terminiert wird (durch den standardmäßig
   *  installierten unexpected_handler).
   *
   *  Ein terminate_handler muss das Programm beenden ohne die Kontrolle zum
   *  Aufrufer zurückzugeben
   *
   *  Der standardmäßig installierte terminate_handler ruft abort() auf.
   *\endgerman
   */
  typedef void (*terminate_handler)() _LIGHTLIBCPP_NORETURN;

  /**
   *  18.8.3.2 set_terminate
   *
   *\english
   *  The terminate_handler f becomes the new handler called by terminate()
   *  \param[in] f a non-null pointer to the new terminate_handler
   *  \return a pointer to the previous terminate_handler
   *\endenglish
   *\german
   *  Der terminate_handler f wird der neue Handler, der von terminate()
   *  aufgerufen wird
   *  \param[in] f ein gültiger Zeiger auf einen terminate_handler
   *  \return ein Zeiger auf den vorhergehende terminate_handler
   *\endgerman
   */
  terminate_handler set_terminate(terminate_handler f) throw();

  /**
   *\english
   *  lightlibc++ extension
   *
   *  Get the current terminate_handler
   *  \note This function is needed by the C++ exception handling ABI
   *  \return apointer to the current terminate_handler
   *\endenglish
   *\german
   *  lightlibc++ Erweiterung
   *
   *  Gibt einen Zeiger auf momentan installierten terminate_handler zurück
   *  \note Diese Funktion wird von der C++ Ausnahmebehandlungs-ABI benötigt
   *  \return ein Zeiger auf den momentan installierten terminate_handler
   *\endgerman
   */
  terminate_handler __get_terminate();

  /**
   *  18.8.3.3 terminate
   *
   *\english
   *  Called by the default unexpected_handler. May also be called directly by
   *  the program.
   *
   *  Calls the calls the current terminate_handler.
   *\endenglish
   *\german
   *  Diese Funktion wird vom standardmäßig installierten unexpected_handler
   *  aufgerufen. Außerdem darf terminate() auch direkt aus dem Programm
   *  aufgerufen werden.
   *
   *  terminate() ruft den momentanen terminate_handler auf.
   *\endgerman
   */
  void terminate() _LIGHTLIBCPP_NORETURN;

  /*@}*/



  /** \addtogroup lightlibcpp_18_8 */
  /*@{*/

  // TODO:  bool uncaught_exception() throw(), implemented by the C++ ABI: Exception Handling patch

  #ifdef _LIGHTLIBCPP_CPP10
    // 18.8.5 Exception Propagation
    // TODO C++1x: typedef \unspec exception_ptr;
    // TODO C++1x: exception_ptr current_exception();
    // TODO C++1x: void rethrow_exception(exception_ptr p);
    // TODO C++1x: template<class E> exception_ptr copy_exception(E e);

    // TODO 18.8.6 class nested_exception
    // TODO template<class T> void throw_with_nested(T&& t); // [[noreturn]]
    // TODO template <class E> void rethrow_if_nested(const E& e);
  #endif

  /*@}*/
}



#endif
