/*
Permission is granted to use, modify, and / or redistribute at will.

This includes removing authorship notices, re-use of code parts in
other software (with or without giving credit), and / or creating a
commercial product based on it.

This permission is not revocable by the author.

This software is provided as-is. Use it at your own risk. There is
no warranty whatsoever, neither expressed nor implied, and by using
this software you accept that the author(s) shall not be held liable
for any loss of data, loss of service, or other damages, be they
incidental or consequential. Your only option other than accepting
this is not to use the software at all.
*/
#ifndef _LIGHTLIBCPP_NEW_HPP
#define _LIGHTLIBCPP_NEW_HPP



#include <lightlibc++/compiler.hpp>
#include <cstddef>           // std::size_t
#ifndef _LIGHTLIBCPP_NO_EXCEPTIONS
  #include <exception>       // std::exception
#endif



namespace std
{
#ifndef _LIGHTLIBCPP_NO_EXCEPTIONS
  /* (Forward) declaration of std::bad_alloc */
  class bad_alloc;
#endif



  /** \addtogroup lightlibcpp_18_6 */
  /*@{*/

  /**
   *\english
   *  \brief Support structure for nothrow-new operator
   *
   *  Support structure for the usage of the nothrow-new operator. This
   *  structure is intentionally empty.
   *\endenglish
   *\german
   *  \brief Struktur zur Unterstützung des nothrow-new Operators
   *
   *  Die Struktur ermöglicht das Benutzen des nothrow-new Operators und ist
   *  absichtlich leer.
   *\endgerman */
  struct nothrow_t
  {
  };

  /**
   *\english
   *  External instance of the nothrow_t structure.
   *\endenglish
   *\german
   *  Externe Instanz der nothrow_t-Struktur.
   *\endgerman */
  extern const nothrow_t nothrow;

  /*@}*/



  /** \addtogroup lightlibcpp_18_6_2 */
  /*@{*/

#ifndef _LIGHTLIBCPP_NO_EXCEPTIONS

  /**
   *\english
   *  \brief Exception to report failure to allocate storage
   *
   *  18.6.2.1 Class bad_alloc
   *
   *  The class bad_alloc deﬁnes the type of objects thrown as exceptions by
   *  the implementation to report a failure to allocate storage.
   *\endenglish
   *\german
   *  \brief Ausnahme um Fehlschlag einer Speicherallokation anzuzeigen
   *
   *  18.6.2.1 Klasse bad_alloc
   *
   *  Die Klasse bad_alloc definiert den Objekttyp, welcher als Ausnahme
   *  geworfen wird, um den Fehlschlag einer Speicherallokation anzuzeigen.
   *\endgerman */
  class bad_alloc : public std::exception
  {
    public:
      /**
       *\english
       *  Constructs an object of class bad_alloc.
       *\endenglish
       *\german
       *  Konstruiert ein Objekt der Klasse bad_alloc.
       *\endgerman */
      inline bad_alloc() throw()
      {
      }

      /**
       *\english
       *  Copies an object of class bad_alloc.
       *  \param[in] x reference to the object to copy
       *\endenglish
       *\german
       *  Kopiert ein Objekt der Klasse bad_alloc.
       *  \param[in] x Referenz auf das zu kopierende Objekt
       *\endgerman */
      inline bad_alloc(const bad_alloc& x) throw()
        : std::exception(x)
      {
      }

      /**
       *\english
       *  Destructs an object of class bad_alloc
       *\endenglish
       *\german
       *  Zerstört ein Objekt der Klasse bad_alloc
       *\endgerman */
      virtual ~bad_alloc() throw();

      virtual const char* what() const throw();
  };

#endif

  /**
   *\english
   *  18.6.2.2 Type new_handler
   *
   *  The type of a handler function to be called by operator the new operators
   *  when they cannot satisfy a request for additional storage.
   *
   *  Required behavior: A new_handler shall perform one of the following:
   *   - make more storage available for allocation and then return
   *   - throw an exception of type bad_alloc or a class derived from bad_alloc
   *   - call either abort() or exit()
   *\endenglish
   *\german
   *  18.6.2.2 Typ new_handler
   *
   *  Der Typ einer Handlerfunktion die von den new-Operatoren aufgerufen wird,
   *  wenn dieser eine Speicherallokation nicht erfüllen kann. Der Handler
   *  muss eine der folgenden Bedinungen erfüllen:
   *   - mehr Speicher zur Verfügung stellen und zur aufrufenden Funktion
   *     zurückkehren
   *   - eine Ausnahme vom Typ bad_alloc oder einer davon abgeleiteten Klasse
   *     werfen
   *   - entweder abort() oder exit() aufrufen
   *\endgerman */
  typedef void (*new_handler)();

  /** 18.6.2.3 set_new_handler
   *\english
   *  Establishes the function designated by new_p as the current new_handler.
   *  \param[in] new_p the new new_handler
   *  \return 0 on the ﬁrst call, the previous new_handler on subsequent
   *          calls.
   *\endenglish
   *\german
   *  Die Funktion new_p wird zum neuen Handler new_handler
   *  \param[in] new_p der neue new_handler
   *  \return 0 beim ersten Aufruf oder vorhergehender new_handler bei den
   *          darauf folgenden Aufrufen
   *\endgerman */
  new_handler set_new_handler(new_handler new_p) throw();

  /*@}*/
}



/** \addtogroup lightlibcpp_18_6_1 */
/*@{*/

/* 18.6.1.1 Single-object forms */

/**
 *\english
 *  The allocation function called by a new-expression to allocate size bytes
 *  suitably aligned to represent any object of that size. If the allocation
 *  request cannot be satisfied bad_alloc is thrown (if
 *  _LIGHTLIBCPP_NO_EXCEPTIONS is not defined).
 *
 *  The default behaviour is the following:
 *
 *  1. call malloc
 *
 *  2. return the pointer to the allocated space if malloc succeeded
 *
 *  3. throw bad_alloc if no new_handler is installed
 *
 *  4. call the new_handler and go to 1
 *  \note A C++ program may define a function with this function signature that
 *        displaces the default version defined by the C++ Standard library,
 *        but the above behaviour is required.
 *  \param[in] size number of bytes to allocate
 *  \return a pointer to the allocated storage
 *\endenglish
 *\german
 *  Die Allokationsfunktion, die von einem new-Ausdruck aufgerufen wird, um
 *  size Bytes mit dem richtigen Alignment für alle Objekte dieser Größe zu
 *  allozieren. Falls nicht genügend Speicher vorhanden ist, wird eine
 *  Ausnahme vom Typ bad_alloc geworfen (falls _LIGHTLIBCPP_NO_EXCEPTIONS
 *  nicht definiert ist).
 *
 *  Das Standardverhalten ist:
 *
 *  1. Aufrufen von malloc
 *
 *  2. den Zeiger auf den allozierten Speicherplatz zurückgeben, falls der
 *     malloc-Aufruf erfolgreich war
 *
 *  3. eine Ausnahme vom Typ bad_alloc werfen, falls kein new_handler
 *     installiert ist
 *
 *  4. den momentan installierten new_handler aufrufen und zu 1 zurückgehen
 *  \note Ein C++ Programm darf eine Funktion mit dieser Funktionssignatur,
 *        die die Funktion mit dem Standardverhalten aus der C++
 *        Standardbibliothek ersetzt, definieren, aber das obige Verhalten ist
 *        verpflichtend.
 *  \param[in] size Anzahl der Bytes die alloziert werden sollen
 *  \return ein Zeiger auf den allozierten Speicherplatz
 *\endgerman
 */
void* operator new(std::size_t size)
#ifndef _LIGHTLIBCPP_NO_EXCEPTIONS
   throw(std::bad_alloc);
#else
  throw();
#endif

/**
 *\english
 *  The allocation function called by a placement new-expression to allocate
 *  size bytes suitably aligned to represent any object of that size. If the
 *  allocation request cannot be satisfied 0 is returned.
 *
 *  The default behaviour is the following:
 *
 *  1. call malloc
 *
 *  2. return the pointer to the allocated space if malloc succeeded
 *
 *  3. return 0 if no new_handler is installed
 *
 *  4. call the new_handler and go to 1
 *  \note A C++ program may define a function with this function signature that
 *        displaces the default version defined by the C++ Standard library,
 *        but the above behaviour is required.
 *  \param[in] size number of bytes to allocate
 *  \return a pointer to the allocated storage or 0
 *\endenglish
 *\german
 *  Die Allokationsfunktion, die von einem placement-new-Ausdruck aufgerufen
 *  wird, um size Bytes mit dem richtigen Alignment für alle Objekte dieser
 *  Größe zu allozieren. Falls nicht genügend Speicher vorhanden ist, wird 0
 *  zurückgegeben.
 *
 *  Das Standardverhalten ist:
 *
 *  1. Aufrufen von malloc
 *
 *  2. den Zeiger auf den allozierten Speicherplatz zurückgeben, falls der
 *     malloc-Aufruf erfolgreich war
 *
 *  3. 0 zurückgeben, falls kein new_handler installiert ist
 *
 *  4. den momentan installierten new_handler aufrufen und zu 1 zurückgehen
 *  \note Ein C++ Programm darf eine Funktion mit dieser Funktionssignatur,
 *        die die Funktion mit dem Standardverhalten aus der C++
 *        Standardbibliothek ersetzt, definieren, aber das obige Verhalten ist
 *        verpflichtend.
 *  \param[in] size Anzahl der Bytes die alloziert werden sollen
 *  \return ein Zeiger auf den allozierten Speicherplatz oder 0
 *\endgerman
 */
void* operator new(std::size_t size,
                   const std::nothrow_t&) throw();

/**
 *\english
 *  The deallocation function called by a delete-expression to free the memory
 *  at the address ptr points to. If ptr is 0, nothing is done.
 *
 *  ptr must have been allocated with
 *
 *  * operator new(std::size_t) or with
 *
 *  * operator new(std::size_t, const std::nothrow_t&)
 *  \note A C++ program may define a function with this function signature that
 *        displaces the default version defined by the C++ Standard library,
 *        but the above behaviour is required.
 *  \param[in] ptr the address of the memory supposed to be deallocated
 *\endenglish
 *\german
 *  Die Deallokationsfunktion, welche von einem delete-Ausdruck aufgerufen
 *  wird, um den Speicher an der Adresse auf die ptr zeigt freizugeben. Falls
 *  ptr 0 ist, wird nichts getan.
 *
 *  ptr muss mit
 *
 *  * operator new(std::size_t) oder mit
 *
 *  * operator new(std::size_t, const std::nothrow_t&)
 *
 *  alloziert worden sein.
 *  \note Ein C++ Programm darf eine Funktion mit dieser Funktionssignatur,
 *        die die Funktion mit dem Standardverhalten aus der C++
 *        Standardbibliothek ersetzt, definieren, aber das obige Verhalten ist
 *        verpflichtend.
 *  \param[in] ptr die Adresse des zu deallozierenden Speichers
 *\endgerman
 */
void operator delete(void* ptr) throw();

/**
 *\english
 *  The deallocation function called by a placement delete-expression to free
 *  the memory at the address ptr points to. If ptr is 0, nothing is done.
 *
 *  ptr must have been allocated with
 *
 *  * operator new(std::size_t) or with
 *
 *  * operator new(std::size_t, const std::nothrow_t&)
 *  \note A C++ program may define a function with this function signature that
 *        displaces the default version defined by the C++ Standard library,
 *        but the above behaviour is required.
 *  \param[in] ptr the address of the memory supposed to be deallocated
 *\endenglish
 *\german
 *  Die Deallokationsfunktion, welche von einem placement-delete-Ausdruck
 *  aufgerufen wird, um den Speicher an der Adresse auf die ptr zeigt
 *  freizugeben. Falls ptr 0 ist, wird nichts getan.
 *
 *  ptr muss mit
 *
 *  * operator new(std::size_t) oder mit
 *
 *  * operator new(std::size_t, const std::nothrow_t&)
 *
 *  alloziert worden sein.
 *  \note Ein C++ Programm darf eine Funktion mit dieser Funktionssignatur,
 *        die die Funktion mit dem Standardverhalten aus der C++
 *        Standardbibliothek ersetzt, definieren, aber das obige Verhalten ist
 *        verpflichtend.
 *  \param[in] ptr die Adresse des zu deallozierenden Speichers
 *\endgerman
 */
void operator delete(void* ptr,
                     const std::nothrow_t&) throw();



/* 18.6.1.2 Array forms */

/**
 *\english
 *  The allocation function called by the array form of a new-expression to
 *  allocate size bytes suitably aligned to represent any object of that size.
 *  If the allocation request cannot be satisfied bad_alloc is thrown (if
 *  _LIGHTLIBCPP_NO_EXCEPTIONS is not defined).
 *
 *  default behaviour: return operator new(size)
 *  \note A C++ program may define a function with this function signature that
 *        displaces the default version defined by the C++ Standard library,
 *        but the above behaviour is required.
 *  \param[in] size number of bytes to allocate
 *  \return a pointer to the allocated storage
 *\endenglish
 *\german
 *  Die Allokationsfunktion, die von der Array-Version des new-Ausdruck
 *  aufgerufen wird, um size Bytes mit dem richtigen Alignment für alle Objekte
 *  dieser Größe zu allozieren. Falls nicht genügend Speicher vorhanden ist,
 *  wird eine Ausnahme vom Typ bad_alloc geworfen (falls
 *  _LIGHTLIBCPP_NO_EXCEPTIONS nicht definiert ist).
 *
 *  Das Standardverhalten ist: return operator new(size)
 *  \note Ein C++ Programm darf eine Funktion mit dieser Funktionssignatur,
 *        die die Funktion mit dem Standardverhalten aus der C++
 *        Standardbibliothek ersetzt, definieren, aber das obige Verhalten ist
 *        verpflichtend.
 *  \param[in] size Anzahl der Bytes, die alloziert werden sollen
 *  \return ein Zeiger auf den allozierten Speicherplatz
 *\endgerman
 */
void* operator new[](std::size_t size)
#ifndef _LIGHTLIBCPP_NO_EXCEPTIONS
  throw(std::bad_alloc);
#else
  throw();
#endif

/**
 *\english
 *  The allocation function called by the array-version of a placement
 *  new-expression to allocate size bytes suitably aligned to represent any
 *  object of that size. If the allocation request cannot be satisfied 0 is
 *  returned.
 *
 *  The default behaviour is operator new (size, std::nothrow)
 *  \note A C++ program may define a function with this function signature that
 *        displaces the default version defined by the C++ Standard library,
 *        but the above behaviour is required.
 *  \param[in] size number of bytes to allocate
 *  \return a pointer to the allocated storage or 0
 *\endenglish
 *\german
 *  Die Allokationsfunktion, die von der Array-Version eines
 *  placement-new-Ausdruck aufgerufen wird, um size Bytes mit dem richtigen
 *  Alignment für alle Objekte dieser Größe zu allozieren. Falls nicht genügend
 *  Speicher vorhanden ist, wird 0 zurückgegeben.
 *
 *  Das Standardverhalten ist: return operator new(size, std::nothrow)
 *  \note Ein C++ Programm darf eine Funktion mit dieser Funktionssignatur,
 *        die die Funktion mit dem Standardverhalten aus der C++
 *        Standardbibliothek ersetzt, definieren, aber das obige Verhalten ist
 *        verpflichtend.
 *  \param[in] size Anzahl der Bytes die alloziert werden sollen
 *  \return ein Zeiger auf den allozierten Speicherplatz oder 0
 *\endgerman
 */
void* operator new[](std::size_t size,
                     const std::nothrow_t&) throw();

/**
 *\english
 *  The deallocation function called by the array-verion of a delete-expression
 *  to free the memory at the address ptr points to. If ptr is 0, nothing is
 *  done.
 *
 *  ptr must have been allocated with
 *
 *  * operator new[](std::size_t) or with
 *
 *  * operator new[](std::size_t, const std::nothrow_t&)
 *  \note A C++ program may define a function with this function signature that
 *        displaces the default version defined by the C++ Standard library,
 *        but the above behaviour is required.
 *  \param[in] ptr the address of the memory supposed to be deallocated
 *\endenglish
 *\german
 *  Die Deallokationsfunktion, welche von der Array-Version eines
 *  delete-Ausdrucks aufgerufen wird, um den Speicher an der Adresse auf die
 *  ptr zeigt freizugeben. Falls ptr 0 ist, wird nichts getan.
 *
 *  ptr muss mit
 *
 *  * operator new[](std::size_t) oder mit
 *
 *  * operator new[](std::size_t, const std::nothrow_t&)
 *
 *  alloziert worden sein.
 *  \note Ein C++ Programm darf eine Funktion mit dieser Funktionssignatur,
 *        die die Funktion mit dem Standardverhalten aus der C++
 *        Standardbibliothek ersetzt, definieren, aber das obige Verhalten ist
 *        verpflichtend.
 *  \param[in] ptr die Adresse des zu deallozierenden Speichers
 *\endgerman
 */
void operator delete[](void* ptr) throw();

/**
 *\english
 *  The deallocation function called by the array-version of a placement
 *  delete-expression to free the memory at the address ptr points to. If ptr
 *  is 0, nothing is done.
 *
 *  ptr must have been allocated with
 *
 *  * operator new[](std::size_t) or with
 *
 *  * operator new[](std::size_t, const std::nothrow_t&)
 *  \note A C++ program may define a function with this function signature that
 *        displaces the default version defined by the C++ Standard library,
 *        but the above behaviour is required.
 *  \param[in] ptr the address of the memory supposed to be deallocated
 *\endenglish
 *\german
 *  Die Deallokationsfunktion, welche von der Array-Version eines
 *  placement-delete-Ausdrucks aufgerufen wird, um den Speicher an der Adresse
 *  auf die ptr zeigt freizugeben. Falls ptr 0 ist, wird nichts getan.
 *
 *  ptr muss mit
 *
 *  * operator new[](std::size_t) oder mit
 *
 *  * operator new[](std::size_t, const std::nothrow_t&)
 *
 *  alloziert worden sein.
 *  \note Ein C++ Programm darf eine Funktion mit dieser Funktionssignatur,
 *        die die Funktion mit dem Standardverhalten aus der C++
 *        Standardbibliothek ersetzt, definieren, aber das obige Verhalten ist
 *        verpflichtend.
 *  \param[in] ptr die Adresse des zu deallozierenden Speichers
 *\endgerman
 */
void operator delete[](void* ptr,
                       const std::nothrow_t&) throw();



/* 18.6.1.3 Placement forms */

/**
 *\english
 *  Intentionally does nothing
 *  \param[in] ptr  returned unchanged
 *  \return ptr
 *  \note A C++ program may not define a function that displaces this function
 *        in the Standard C++ library.
 *\endenglish
 *\german
 *  Tut absichtlich nichts
 *  \param[in] ptr  wird unverändert zurückgegeben
 *  \return ptr
 *  \note Ein C++ Programm darf keine Funktion definieren, welche diese
 *        Funktion der Standard C++ Bibliothek ersetzt.
 *\endgerman */
void* operator new(std::size_t,
                   void* ptr) throw();

/**
 *\english
 *  Intentionally does nothing
 *  \param[in] ptr  returned unchanged
 *  \return ptr
 *  \note A C++ program may not define a function that displaces this function
 *        in the Standard C++ library.
 *\endenglish
 *\german
 *  Tut absichtlich nichts
 *  \param[in] ptr  wird unverändert zurückgegeben
 *  \return ptr
 *  \note Ein C++ Programm darf keine Funktion definieren, welche diese
 *        Funktion der Standard C++ Bibliothek ersetzt.
 *\endgerman */
void* operator new[](std::size_t,
                     void* ptr) throw();

/**
 *\english
 *  Intentionally does nothing
 *  \note A C++ program may not define a function that displaces this function
 *        in the Standard C++ library.
 *\endenglish
 *\german
 *  Tut absichtlich nichts
 *  \note Ein C++ Programm darf keine Funktion definieren, welche diese
 *        Funktion der Standard C++ Bibliothek ersetzt.
 *\endgerman */
void operator delete(void*,
                     void*) throw();

/**
 *\english
 *  Intentionally does nothing
 *  \note A C++ program may not define a function that displaces this function
 *        in the Standard C++ library.
 *\endenglish
 *\german
 *  Tut absichtlich nichts
 *  \note Ein C++ Programm darf keine Funktion definieren, welche diese
 *        Funktion der Standard C++ Bibliothek ersetzt.
 *\endgerman */
void operator delete[](void*,
                       void*) throw();

/*@}*/



#endif
