/*
Permission is granted to use, modify, and / or redistribute at will.

This includes removing authorship notices, re-use of code parts in
other software (with or without giving credit), and / or creating a
commercial product based on it.

This permission is not revocable by the author.

This software is provided as-is. Use it at your own risk. There is
no warranty whatsoever, neither expressed nor implied, and by using
this software you accept that the author(s) shall not be held liable
for any loss of data, loss of service, or other damages, be they
incidental or consequential. Your only option other than accepting
this is not to use the software at all.
*/
#ifndef _LIGHTLIBCPP_LIMITS_HPP
#define _LIGHTLIBCPP_LIMITS_HPP



#include <lightlibc++/compiler.hpp>
#include <climits>
#define __STDC_LIMIT_MACROS 1
#include <cstdint>


namespace std
{
#ifndef _LIGHTLIBCPP10_CPP10_NO_LONG_LONG
  typedef unsigned long long __max_builtin_type;
#else
  typedef unsigned long __max_builtin_type;
#endif

  template<__max_builtin_type x>
  struct __log10
  {
    const static __max_builtin_type value = 1 + __log10<(x / 10)>::value;
  };

  template<>struct __log10<0>{const static __max_builtin_type value = 0;};
  template<>struct __log10<1>{const static __max_builtin_type value = 0;};
  template<>struct __log10<2>{const static __max_builtin_type value = 0;};
  template<>struct __log10<3>{const static __max_builtin_type value = 0;};
  template<>struct __log10<4>{const static __max_builtin_type value = 0;};
  template<>struct __log10<5>{const static __max_builtin_type value = 0;};
  template<>struct __log10<6>{const static __max_builtin_type value = 0;};
  template<>struct __log10<7>{const static __max_builtin_type value = 0;};
  template<>struct __log10<8>{const static __max_builtin_type value = 0;};
  template<>struct __log10<9>{const static __max_builtin_type value = 0;};



  enum float_round_style
  {
    round_indeterminate                = -1,
    round_toward_zero                  = 0,
    round_to_nearest                   = 1,
    round_toward_infinity              = 2,
    round_toward_neg_infinity          = 3
  };

  enum float_denorm_style
  {
    denorm_indeterminate               = -1,
    denorm_absent                      = 0,
    denorm_present                     = 1
  };

  template<class T>
  class __numeric_limits_base
  {
    public:
      static _LIGHTLIBCPP_CONSTEXPR bool is_specialized = false;
      static _LIGHTLIBCPP_CONSTEXPR_F T min() throw()
      {
        return T();
      }
      static _LIGHTLIBCPP_CONSTEXPR_F T max() throw()
      {
        return T();
      }
      static _LIGHTLIBCPP_CONSTEXPR_F T lowest() throw()
      {
        return T();
      }
      static _LIGHTLIBCPP_CONSTEXPR int digits = 0;
      static _LIGHTLIBCPP_CONSTEXPR int digits10 = 0;
      static _LIGHTLIBCPP_CONSTEXPR int max_digits10 = 0;
      static _LIGHTLIBCPP_CONSTEXPR bool is_signed = false;
      static _LIGHTLIBCPP_CONSTEXPR bool is_integer = false;
      static _LIGHTLIBCPP_CONSTEXPR bool is_exact = false;
      static _LIGHTLIBCPP_CONSTEXPR int radix = 0;
      static _LIGHTLIBCPP_CONSTEXPR_F T epsilon() throw()
      {
        return T();
      }
      static _LIGHTLIBCPP_CONSTEXPR_F T round_error() throw()
      {
        return T();
      }
      static _LIGHTLIBCPP_CONSTEXPR int min_exponent = 0;
      static _LIGHTLIBCPP_CONSTEXPR int min_exponent10 = 0;
      static _LIGHTLIBCPP_CONSTEXPR int max_exponent = 0;
      static _LIGHTLIBCPP_CONSTEXPR int max_exponent10 = 0;
      static _LIGHTLIBCPP_CONSTEXPR bool has_infinity = false;
      static _LIGHTLIBCPP_CONSTEXPR bool has_quiet_NaN = false;
      static _LIGHTLIBCPP_CONSTEXPR bool has_signaling_NaN = false;
      static _LIGHTLIBCPP_CONSTEXPR float_denorm_style had_denorm = denorm_absent;
      static _LIGHTLIBCPP_CONSTEXPR bool has_denorm_loss = false;
      static _LIGHTLIBCPP_CONSTEXPR_F T infinity() throw()
      {
        return T();
      }
      static _LIGHTLIBCPP_CONSTEXPR_F T quiet_NaN() throw()
      {
        return T();
      }
      static _LIGHTLIBCPP_CONSTEXPR_F T signaling_NaN() throw()
      {
        return T();
      }
      static _LIGHTLIBCPP_CONSTEXPR_F T denorm_min() throw()
      {
        return T();
      }
      static _LIGHTLIBCPP_CONSTEXPR bool is_iec559 = false;
      static _LIGHTLIBCPP_CONSTEXPR bool is_bounded = false;
      static _LIGHTLIBCPP_CONSTEXPR bool is_modulo = false;
      static _LIGHTLIBCPP_CONSTEXPR bool traps = false;
      static _LIGHTLIBCPP_CONSTEXPR bool tinyness_before = false;
      static _LIGHTLIBCPP_CONSTEXPR float_round_style round_style = round_toward_zero;
  };

  template<class T>
  class __numeric_limits_integer_base : public __numeric_limits_base<T>
  {
    public:
      static _LIGHTLIBCPP_CONSTEXPR bool is_specialized = true;
      static _LIGHTLIBCPP_CONSTEXPR bool is_integer     = true;
      static _LIGHTLIBCPP_CONSTEXPR bool is_exact       = true;
      static _LIGHTLIBCPP_CONSTEXPR bool is_bounded     = true;
      static _LIGHTLIBCPP_CONSTEXPR int radix           = 2;
  };

  template<class T>
  class numeric_limits : public __numeric_limits_base<T>
  {
  };

  template<class T>
  class numeric_limits<const T> : public numeric_limits<T>
  {
  };

  template<class T>
  class numeric_limits<volatile T> : public numeric_limits<T>
  {
  };

  template<class T>
  class numeric_limits<const volatile T> : public numeric_limits<T>
  {
  };

  template<>
  class numeric_limits<bool> : public __numeric_limits_integer_base<bool>
  {
    public:
      static _LIGHTLIBCPP_CONSTEXPR_F bool min() throw()
      {
        return false;
      }
      static _LIGHTLIBCPP_CONSTEXPR_F bool max() throw()
      {
        return true;
      }
      static _LIGHTLIBCPP_CONSTEXPR_F bool lowest() throw()
      {
        return false;
      }
      static _LIGHTLIBCPP_CONSTEXPR int digits = 1;
  };

  template<>
  class numeric_limits<char> : public __numeric_limits_integer_base<char>
  {
    public:
    #if CHAR_MIN != 0
      static _LIGHTLIBCPP_CONSTEXPR bool is_signed = true;
    #endif
      static _LIGHTLIBCPP_CONSTEXPR_F char min() throw()
      {
        return CHAR_MIN;
      }
      static _LIGHTLIBCPP_CONSTEXPR_F char max() throw()
      {
        return CHAR_MAX;
      }
      static _LIGHTLIBCPP_CONSTEXPR_F char lowest() throw()
      {
        return CHAR_MIN;
      }
    #if CHAR_MIN == 0
      static _LIGHTLIBCPP_CONSTEXPR int digits = CHAR_BIT;
    #else
      static _LIGHTLIBCPP_CONSTEXPR int digits = CHAR_BIT - 1;
    #endif
      static _LIGHTLIBCPP_CONSTEXPR int digits10 = std::__log10<CHAR_MAX>::value;
      static _LIGHTLIBCPP_CONSTEXPR bool is_modulo = true;
  };

  template<>
  class numeric_limits<signed char> : public __numeric_limits_integer_base<signed char>
  {
    public:
      static _LIGHTLIBCPP_CONSTEXPR bool is_signed = true;
      static _LIGHTLIBCPP_CONSTEXPR_F signed char min() throw()
      {
        return SCHAR_MIN;
      }
      static _LIGHTLIBCPP_CONSTEXPR_F signed char max() throw()
      {
        return SCHAR_MAX;
      }
      static _LIGHTLIBCPP_CONSTEXPR_F signed char lowest() throw()
      {
        return SCHAR_MIN;
      }
      static _LIGHTLIBCPP_CONSTEXPR int digits = CHAR_BIT - 1;
      static _LIGHTLIBCPP_CONSTEXPR int digits10 = std::__log10<SCHAR_MAX>::value;
      static _LIGHTLIBCPP_CONSTEXPR bool is_modulo = true;
  };

  template<>
  class numeric_limits<unsigned char> : public __numeric_limits_integer_base<unsigned char>
  {
    public:
      static _LIGHTLIBCPP_CONSTEXPR_F unsigned char min() throw()
      {
        return 0;
      }
      static _LIGHTLIBCPP_CONSTEXPR_F unsigned char max() throw()
      {
        return UCHAR_MAX;
      }
      static _LIGHTLIBCPP_CONSTEXPR_F unsigned char lowest() throw()
      {
        return 0;
      }
      static _LIGHTLIBCPP_CONSTEXPR int digits = CHAR_BIT;
      static _LIGHTLIBCPP_CONSTEXPR int digits10 = std::__log10<UCHAR_MAX>::value;
      static _LIGHTLIBCPP_CONSTEXPR bool is_modulo = true;
  };
  

#ifndef _LIGHTLIBCPP_CPP10_NO_CHAR16_T
  // template<>
  // TODO class numeric_limits<char16_t>;
#endif

#ifndef _LIGHTLIBCPP_CPP10_NO_CHAR32_T
  // template<>
  // TODO class numeric_limits<char32_t>;
#endif

  template<>
  class numeric_limits<wchar_t> : public __numeric_limits_integer_base<wchar_t>
  {
    #if WCHAR_MIN != 0
      static _LIGHTLIBCPP_CONSTEXPR bool is_signed = true;
    #endif
      static _LIGHTLIBCPP_CONSTEXPR_F wchar_t min() throw()
      {
        return WCHAR_MIN;
      }
      static _LIGHTLIBCPP_CONSTEXPR_F wchar_t max() throw()
      {
        return WCHAR_MAX;
      }
      static _LIGHTLIBCPP_CONSTEXPR_F wchar_t lowest() throw()
      {
        return WCHAR_MIN;
      }
    #if WCHAR_MIN == 0
      static _LIGHTLIBCPP_CONSTEXPR int digits = CHAR_BIT * sizeof(wchar_t);
    #else
      static _LIGHTLIBCPP_CONSTEXPR int digits = CHAR_BIT * sizeof(wchar_t) - 1;
    #endif
      static _LIGHTLIBCPP_CONSTEXPR int digits10 = std::__log10<WCHAR_MAX>::value;
      static _LIGHTLIBCPP_CONSTEXPR bool is_modulo = true;
  };

  template<>
  class numeric_limits<short> : public __numeric_limits_integer_base<short>
  {
    public:
      static _LIGHTLIBCPP_CONSTEXPR bool is_signed = true;
      static _LIGHTLIBCPP_CONSTEXPR_F short min() throw()
      {
        return SHRT_MIN;
      }
      static _LIGHTLIBCPP_CONSTEXPR_F short max() throw()
      {
        return SHRT_MAX;
      }
      static _LIGHTLIBCPP_CONSTEXPR_F short lowest() throw()
      {
        return SHRT_MIN;
      }
      static _LIGHTLIBCPP_CONSTEXPR int digits = CHAR_BIT * sizeof(short) - 1;
      static _LIGHTLIBCPP_CONSTEXPR int digits10 = std::__log10<SHRT_MAX>::value;
      static _LIGHTLIBCPP_CONSTEXPR bool is_modulo = true;
  };

  template<>
  class numeric_limits<int> : public __numeric_limits_integer_base<int>
  {
    public:
      static _LIGHTLIBCPP_CONSTEXPR bool is_signed = true;
      static _LIGHTLIBCPP_CONSTEXPR_F int min() throw()
      {
        return INT_MIN;
      }
      static _LIGHTLIBCPP_CONSTEXPR_F int max() throw()
      {
        return INT_MAX;
      }
      static _LIGHTLIBCPP_CONSTEXPR_F int lowest() throw()
      {
        return INT_MIN;
      }
      static _LIGHTLIBCPP_CONSTEXPR int digits = CHAR_BIT * sizeof(int) - 1;
      static _LIGHTLIBCPP_CONSTEXPR int digits10 = std::__log10<INT_MAX>::value;
      static _LIGHTLIBCPP_CONSTEXPR bool is_modulo = true;
  };

  template<>
  class numeric_limits<long> : public __numeric_limits_integer_base<long>
  {
    public:
      static _LIGHTLIBCPP_CONSTEXPR bool is_signed = true;
      static _LIGHTLIBCPP_CONSTEXPR_F long min() throw()
      {
        return LONG_MIN;
      }
      static _LIGHTLIBCPP_CONSTEXPR_F long max() throw()
      {
        return LONG_MAX;
      }
      static _LIGHTLIBCPP_CONSTEXPR_F long lowest() throw()
      {
        return LONG_MIN;
      }
      static _LIGHTLIBCPP_CONSTEXPR int digits = CHAR_BIT * sizeof(long) - 1;
      static _LIGHTLIBCPP_CONSTEXPR int digits10 = std::__log10<LONG_MAX>::value;
      static _LIGHTLIBCPP_CONSTEXPR bool is_modulo = true;
  };

#ifndef _LIGHTLIBCPP_CPP10_NO_LONG_LONG
  template<>
  class numeric_limits<long long> : public __numeric_limits_integer_base<long long>
  {
    public:
      static _LIGHTLIBCPP_CONSTEXPR bool is_signed = true;
      static _LIGHTLIBCPP_CONSTEXPR_F long long min() throw()
      {
        return LLONG_MIN;
      }
      static _LIGHTLIBCPP_CONSTEXPR_F long long max() throw()
      {
        return LLONG_MAX;
      }
      static _LIGHTLIBCPP_CONSTEXPR_F long long lowest() throw()
      {
        return LLONG_MIN;
      }
      static _LIGHTLIBCPP_CONSTEXPR int digits = CHAR_BIT * sizeof(long long) - 1;
      static _LIGHTLIBCPP_CONSTEXPR int digits10 = std::__log10<LLONG_MAX>::value;
      static _LIGHTLIBCPP_CONSTEXPR bool is_modulo = true;
  };
#endif

  template<>
  class numeric_limits<unsigned short> : public __numeric_limits_integer_base<unsigned short>
  {
    public:
      static _LIGHTLIBCPP_CONSTEXPR_F unsigned short min() throw()
      {
        return 0;
      }
      static _LIGHTLIBCPP_CONSTEXPR_F unsigned short max() throw()
      {
        return USHRT_MAX;
      }
      static _LIGHTLIBCPP_CONSTEXPR_F unsigned short lowest() throw()
      {
        return 0;
      }
      static _LIGHTLIBCPP_CONSTEXPR int digits = CHAR_BIT * sizeof(unsigned short);
      static _LIGHTLIBCPP_CONSTEXPR int digits10 = std::__log10<USHRT_MAX>::value;
      static _LIGHTLIBCPP_CONSTEXPR bool is_modulo = true;
  };

  template<>
  class numeric_limits<unsigned int> : public __numeric_limits_integer_base<unsigned int>
  {
    public:
      static _LIGHTLIBCPP_CONSTEXPR_F unsigned int min() throw()
      {
        return 0;
      }
      static _LIGHTLIBCPP_CONSTEXPR_F unsigned int max() throw()
      {
        return UINT_MAX;
      }
      static _LIGHTLIBCPP_CONSTEXPR_F unsigned int lowest() throw()
      {
        return 0;
      }
      static _LIGHTLIBCPP_CONSTEXPR int digits = CHAR_BIT * sizeof(unsigned int);
      static _LIGHTLIBCPP_CONSTEXPR int digits10 = std::__log10<UINT_MAX>::value;
      static _LIGHTLIBCPP_CONSTEXPR bool is_modulo = true;
  };

  template<>
  class numeric_limits<unsigned long> : public __numeric_limits_integer_base<unsigned long>
  {
    public:
      static _LIGHTLIBCPP_CONSTEXPR_F unsigned long min() throw()
      {
        return 0;
      }
      static _LIGHTLIBCPP_CONSTEXPR_F unsigned long max() throw()
      {
        return ULONG_MAX;
      }
      static _LIGHTLIBCPP_CONSTEXPR_F unsigned long lowest() throw()
      {
        return 0;
      }
      static _LIGHTLIBCPP_CONSTEXPR int digits = CHAR_BIT * sizeof(unsigned long);
      static _LIGHTLIBCPP_CONSTEXPR int digits10 = std::__log10<ULONG_MAX>::value;
      static _LIGHTLIBCPP_CONSTEXPR bool is_modulo = true;
  };

#ifndef _LIGHTLIBCPP10_CPP10_NO_LONG_LONG
  template<>
  class numeric_limits<unsigned long long> : public __numeric_limits_integer_base<unsigned long long>
  {
    public:
      static _LIGHTLIBCPP_CONSTEXPR_F unsigned long long min() throw()
      {
        return 0;
      }
      static _LIGHTLIBCPP_CONSTEXPR_F unsigned long long max() throw()
      {
        return ULLONG_MAX;
      }
      static _LIGHTLIBCPP_CONSTEXPR_F unsigned long long lowest() throw()
      {
        return 0;
      }
      static _LIGHTLIBCPP_CONSTEXPR int digits = CHAR_BIT * sizeof(unsigned long long);
      static _LIGHTLIBCPP_CONSTEXPR int digits10 = std::__log10<ULLONG_MAX>::value;
      static _LIGHTLIBCPP_CONSTEXPR bool is_modulo = true;
  };
#endif

  // template<>
  // TODO class numeric_limits<float>;

  // template<>
  // TODO class numeric_limits<double>;

  // template<>
  // TODO class numeric_limits<long double>;
}



#endif
