/*
Permission is granted to use, modify, and / or redistribute at will.

This includes removing authorship notices, re-use of code parts in
other software (with or without giving credit), and / or creating a
commercial product based on it.

This permission is not revocable by the author.

This software is provided as-is. Use it at your own risk. There is
no warranty whatsoever, neither expressed nor implied, and by using
this software you accept that the author(s) shall not be held liable
for any loss of data, loss of service, or other damages, be they
incidental or consequential. Your only option other than accepting
this is not to use the software at all.
*/
#ifndef _LIGHTLIBCPP_COMPILER_HPP
#define _LIGHTLIBCPP_COMPILER_HPP



#ifdef __GNUC__
  /**
  *\english
  *  Used to declare a function that does not return. This allows the compiler
  *  better optimisations.
  *\endenglish
  *\german
  *  Wird benutzt um eine Funktion zu deklarieren, die nicht zum aufrufenden
  *  Code zurückkehrt. Dies erlaubt dem Compiler besser zu optimieren.
  *\endgerman
  */
  #define _LIGHTLIBCPP_NORETURN                            __attribute__((noreturn))

  /**
  *\english
  *  Used to declare classes, functions, variables and types deprecated. When
  *  a deprecated entity is used, the compiler issues a warning or an error
  *\endenglish
  *\german
  *  Wird benutzt um Klassen, Funktionen, Variablen und Typen als veraltet zu
  *  markieren. Bei Benutzung eines dieser Objekte wird der Compiler eine
  *  Warnung oder eine Fehlermeldung ausgeben.
  *\endgerman
  */
  #define _LIGHTLIBCPP_DEPRECATED                          __attribute__((deprecated))

  #ifdef _LIGHTLIBCPP_CPP10_STRICT
    /**
    *\english
    *  See _LIGHTLIBCPP_DEPRECATED for an explanation of what the define does,
    *  This define however is used to indicate that a C++03 feature is
    *  deprecated with C++10.
    *\endenglish
    *\german
    *  Siehe _LIGHTLIBCPP_DEPRECATED. Dieses Makro wird im Gegensatz zu
    *  _LIGHTLIBCPP_DEPRECATED verwendet um anzuzeigen, dass das Feature von
    *   C++03 mit C++10 als veraltet ist.
    *\endgerman
    */
    #define _LIGHTLIBCPP_DEPRECATED_CPP10                  _LIGHTLIBCPP_DEPRECATED
  #else
    #define _LIGHTLIBCPP_DEPRECATED_CPP10
  #endif

  /* Experimental C++10 support (-std=c++0x) */
  #ifdef __GXX_EXPERIMENTAL_CXX0X__
    #define _LIGHTLIBCPP_CPP10                             1
  #else
    #define _LIGHTLIBCPP_CPP03                             1
  #endif

  /* Exceptions (no -fno-exceptions) */
  #ifndef __EXCEPTIONS
    #define _LIGHTLIBCPP_NO_EXCEPTIONS                     1
  #endif

  /* Runtime Type Information (no -fno-rtti) */
  #if __GNUC__ >= 4 && __GNUC_MINOR__ >= 3 && !defined(__GXX_RTTI)
    #define _LIGHTLIBCPP_NO_RTTI                           1
  #endif



  /*
   * C++10 features
   */

  /* long long */
  #if !defined(_LIGHTLIBCPP10)
    #define _LIGHTLIBCPP10_CPP10_NO_LONG_LONG              1
  #endif

  /* C++10 features of gcc 4.3 and later */
  #if !defined(_LIGHTLIBCPP10) || __GNUC__ < 4 || __GNUC_MINOR__ < 3
    #define _LIGHTLIBCPP_CPP10_NO_VARIADIC_TEMPLATES       1
    #define _LIGHTLIBCPP_CPP10_NO_RVALUE_REFERENCES        1
  #endif

  /* C++10 features of gcc 4.4 and later */
  #if !defined(_LIGHTLIBCPP10) || __GNUC__ < 4 || __GNUC_MINOR__ < 4
    #define _LIGHTLIBCPP_CPP10_NO_DEFAULT_DELETE_FUNCTIONS 1
    #define _LIGHTLIBCPP_CPP10_NO_INITIALIZER_LISTS        1
  #endif

  /* Unsupported C++10 features */
  #define _LIGHTLIBCPP_CPP10_NO_CHAR16_T                   1
  #define _LIGHTLIBCPP_CPP10_NO_CHAR32_T                   1
  #define _LIGHTLIBCPP_CPP10_NO_CONSTEXPR                  1



  /* Wrapper around constexpr keyword */
  #ifdef _LIGHTLIBCPP_CPP10_NO_CONSTEXPR
    #define _LIGHTLIBCPP_CONSTEXPR                         const
    #define _LIGHTLIBCPP_CONSTEXPR_F
  #else
    #define _LIGHTLIBCPP_CONSTEXPR                         constexpr
    #define _LIGHTLIBCPP_CONSTEXPR                         constexpt
  #endif

#else
  #error Your compiler is not supported
#endif



#endif
