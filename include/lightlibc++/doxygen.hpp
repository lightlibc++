/*
Permission is granted to use, modify, and / or redistribute at will.

This includes removing authorship notices, re-use of code parts in
other software (with or without giving credit), and / or creating a
commercial product based on it.

This permission is not revocable by the author.

This software is provided as-is. Use it at your own risk. There is
no warranty whatsoever, neither expressed nor implied, and by using
this software you accept that the author(s) shall not be held liable
for any loss of data, loss of service, or other damages, be they
incidental or consequential. Your only option other than accepting
this is not to use the software at all.
*/

/** \defgroup lightlibcpp_18 18. Language support library */
/** \defgroup lightlibcpp_18_2 18.2 Types
 *  \ingroup lightlibcpp_18 */
/** \defgroup lightlibcpp_18_4 18.4 Integer types
 *  \ingroup lightlibcpp_18 */
/** \defgroup lightlibcpp_18_5 18.5 Start and termination
 *  \ingroup lightlibcpp_18 */
/** \defgroup lightlibcpp_18_6 18.6 Dynamic memory management
 *  \ingroup lightlibcpp_18 */
/** \defgroup lightlibcpp_18_6_1 18.6.1 Storage allocation and deallocation
 *  \ingroup lightlibcpp_18_6 */
/** \defgroup lightlibcpp_18_6_2 18.6.2 Storage allocation errors
 *  \ingroup lightlibcpp_18_6 */
/** \defgroup lightlibcpp_18_7 18.7 Type identification
 *  \ingroup lightlibcpp_18 */
/** \defgroup lightlibcpp_18_8 18.8 Exception handling
 *  \ingroup lightlibcpp_18 */
/** \defgroup lightlibcpp_18_8_2 18.8.2 Violating exception-speciﬁcations
 *  \ingroup lightlibcpp_18_8 */
/** \defgroup lightlibcpp_18_8_3 18.8.3 Abnormal termination
 *  \ingroup lightlibcpp_18_8 */
/** \defgroup lightlibcpp_18_9 18.9 Initializer lists
 *  \ingroup lightlibcpp_18 */
/** \defgroup lightlibcpp_18_10 18.10 Other runtime support
 *  \ingroup lightlibcpp_18 */



/** \defgroup lightlibcpp_cxxabi C++ Application Binary Interface */
/** \defgroup lightlibcpp_cxxabi_rtti RTTI (= Runtime Type Information)
 *  \ingroup lightlibcpp_cxxabi */
/** \defgroup lightlibcpp_cxxabi_pure_virtual pure virtual functions
 *  \ingroup lightlibcpp_cxxabi */



/**
 *\english
 *  \brief Namespace of the C++ standard library
 *\endenglish
 *\german
 *  \brief Namensraum der C++ Standardbibliothek
 *\endgerman
 */
namespace std{}
