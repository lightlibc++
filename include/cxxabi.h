/*
Permission is granted to use, modify, and / or redistribute at will.

This includes removing authorship notices, re-use of code parts in
other software (with or without giving credit), and / or creating a
commercial product based on it.

This permission is not revocable by the author.

This software is provided as-is. Use it at your own risk. There is
no warranty whatsoever, neither expressed nor implied, and by using
this software you accept that the author(s) shall not be held liable
for any loss of data, loss of service, or other damages, be they
incidental or consequential. Your only option other than accepting
this is not to use the software at all.
*/
#ifndef _LIGHTLIBCPP_CXXABI_H
#define _LIGHTLIBCPP_CXXABI_H



#include <typeinfo>          // std::type_info



/**
 *\english
 *  \brief internal namespace of the C++ ABI
 *\endenglish
 *\german
 *  \brief intern verwendeter Namensraum der C++ ABI
 *\endgerman
 */
namespace __cxxabiv1
{
#ifndef _LIGHTLIBCPP_NO_RTTI

  /** \addtogroup lightlibcpp_cxxabi_rtti */
  /*@{*/

  /**
   *\english
   *  \brief Base class for the runtime type information of the builtin types
   *\endenglish
   *\german
   *  \brief Basisklasse für die Laufzeittypinformation der eingebauten Typen
   *\endgerman
   */
  class __fundamental_type_info : public std::type_info
  {
    public:
      /**
       *\english
       *  Destructor
       *  \note Needed to generate vtables and type information for this class
       *        and type information for the builtin types
       *\endenglish
       *\german
       *  Destruktor
       *  \note Wird benötigt um vtables und Typinformationen für diese Klasse
       *        und Typinformationen für die eingebauten Typen zu generieren
       *\endgerman
       */
      virtual ~__fundamental_type_info();
  };

  /**
   *\english
   *  \brief Base class for the runtime type information of array types
   *\endenglish
   *\german
   *  \brief Basisklasse für die Laufzeittypinformationen von Arraytypen
   *\endgerman
   */
  class __array_type_info : public std::type_info
  {
    public:
      /**
       *\english
       *  Destructor
       *  \note Needed to generate vtables and type information for this class
       *\endenglish
       *\german
       *  Destruktor
       *  \note Wird benötigt um vtables und Typinformationen für diese Klasse
       *        zu generieren
       *\endgerman
       */
      virtual ~__array_type_info();
  };

  /**
   *\english
   *  \brief Base class for the runtime type information of (non-member)
   *         functions
   *\endenglish
   *\german
   *  \brief Basisklasse für die Laufzeittypinformationen von (nicht-member)
   *         Funktionen
   *\endgerman
   */
  class __function_type_info : public std::type_info
  {
    public:
      /**
       *\english
       *  Destructor
       *  \note Needed to generate vtables and type information for this class
       *\endenglish
       *\german
       *  Destruktor
       *  \note Wird benötigt um vtables und Typinformationen für diese Klasse
       *        zu generieren
       *\endgerman
       */
      virtual ~__function_type_info();
  };

  /**
   *\english
   *  \brief Base class for the runtime type information of enumeration types
   *\endenglish
   *\german
   *  \brief Basisklasse für die Laufzeittypinformationen von Enumerationstypen
   *\endgerman
   */
  class __enum_type_info : public std::type_info
  {
    public:
      /**
       *\english
       *  Destructor
       *  \note Needed to generate vtables and type information for this class
       *\endenglish
       *\german
       *  Destruktor
       *  \note Wird benötigt um vtables und Typinformationen für diese Klasse
       *        zu generieren
       *\endgerman
       */
      virtual ~__enum_type_info();
  };

  /**
   *\english
   *  \brief Base class for the runtime type information of class types
   *\endenglish
   *\german
   *  \brief Basiskalsse für die Laufzeittypinformationen von Klassentypen
   *\endgerman
   */
  class __class_type_info : public std::type_info
  {
    public:
      /**
       *\english
       *  Destructor
       *  \note Needed to generate vtables and type information for this class
       *\endenglish
       *\german
       *  Destruktor
       *  \note Wird benötigt um vtables und Typinformationen für diese Klasse
       *        zu generieren
       *\endgerman
       */
      virtual ~__class_type_info();
  };

  /**
   *\english
   *  \brief Base class for the runtime type information of class types with
   *         single inheritance
   *\endenglish
   *\german
   *  \brief Basisklasse für die Laufzeittypinformationen von Klassentypen mit
   *         Einfachvererbung
   *\endgerman
   */
  class __si_class_type_info : public __class_type_info
  {
    public:
      /**
       *\english
       *  pointer to the type information of the base class
       *\endenglish
       *\german
       *  Zeiger auf die Typinformationen der Basisklasse
       *\endgerman
       */
      const __class_type_info* __base_type;

      /**
       *\english
       *  Destructor
       *  \note Needed to generate vtables and type information for this class
       *\endenglish
       *\german
       *  Destruktor
       *  \note Wird benötigt um vtables und Typinformationen für diese Klasse
       *        zu generieren
       *\endgerman
       */
      virtual ~__si_class_type_info();
  };

  /**
   *\english
   *  \brief Information about a base class in a inheritance hierarchy with
   *         multiple inheritance
   *\endenglish
   *\german
   *  \brief Informationen über eine Basisklasse in einer Vererbungshierarchie
   *         mit Mehrfachvererbung
   *\endgerman
   */
  struct __base_class_type_info
  {
    public:
      /**
       *\english
       *  pointer to the type information of the base class
       *\endenglish
       *\german
       *  Zeiger auf die Typinformationen der Basisklasse
       *\endgerman
       */
      const __class_type_info* __base_type;

      /**
       *\english
       *  flags describing the precise type of the inheritance, see
       *  __base_class_type_info::__offset_flags_masks
       *\endenglish
       *\german
       *  Flags die den genauen Vererbungstyp angeben, siehe
       *  __base_class_type_info::__offset_flags_masks
       *\endgerman
       */
      long                     __offset_flags;

      /** \todo document */
      enum __offset_flags_masks
      {
        /** \todo document */
        __virtual_mask = 0x01,
        /** \todo document */
        __public_mask  = 0x02,
        /** \todo document */
        __offset_shift = 8
      };
  };

  /** \todo document */
  class __vmi_class_type_info : public __class_type_info
  {
    public:
      /** \todo document */
      unsigned int __flags;
      /** \todo document */
      unsigned int __base_count;
      /** \todo document */
      __base_class_type_info __base_info[1];

      /** \todo document */
      enum __flags_masks
      {
        /** \todo document */
        __non_diamond_repeat_mask = 0x01,
        /** \todo document */
        __diamond_shaped_mask     = 0x02
      };

      /**
       *\english
       *  Destructor
       *  \note Needed to generate vtables and type information for this class
       *\endenglish
       *\german
       *  Destruktor
       *  \note Wird benötigt um vtables und Typinformationen für diese Klasse
       *        zu generieren
       *\endgerman
       */
      virtual ~__vmi_class_type_info();
  };

  /**
   *\english
   *  \brief Base class for the runtime type information of pointer types
   *         except pointer to non-member functions
   *\endenglish
   *\german
   *  \brief Basisklasse für die Laufzeittypinformationen von Zeigertypen
   *         außer Zeiger auf nicht-Memberfunktionen
   *\endgerman
   */
  class __pbase_type_info : public std::type_info
  {
    public:
      /**
       *\english
       *  flags describing the precise type of the pointer, see __pbase_type_info::__masks
       *\endenglish
       *\german
       *  Flags die den genauen Typ des Zeigers angeben, siehe __pbase_type_info::__masks
       *\endgerman
       */
      unsigned int          __flags;

      /**
       *\english
       *  pointer to the type information of the type pointed to
       *\endenglish
       *\german
       *  Zeiger auf die Typinformationen des Typs auf den der Zeiger zeigt
       *\endgerman
       */
      const std::type_info* __pointee;

      /**
       *\english
       *  flags of the __flags variable
       *\endenglish
       *\german
       *  Flags für die __flags Variable
       *\endgerman
       */
      enum __masks
      {
        /** const */
        __const_mask            = 0x01,

        /** volatile */
        __volatile_mask         = 0x02,

        /** restrict */
        __restrict_mask         = 0x04,

        /**
         *\english
         *  incomplete type information
         *\endenglish
         *\german
         *  unvollständige Typinformationen
         *\endgerman
         */
        __incomplete_mask       = 0x08,

        /**
         *\english
         *  incomplete type information for a class type
         *\endenglish
         *\german
         *  unvollständige Typinformationen für einen Klassentyp
         *\endgerman
         */
        __incomplete_class_mask = 0x10
      };

      /**
       *\english
       *  Destructor
       *  \note Needed to generate vtables and type information for this class
       *\endenglish
       *\german
       *  Destruktor
       *  \note Wird benötigt um vtables und Typinformationen für diese Klasse
       *        zu generieren
       *\endgerman
       */
      virtual ~__pbase_type_info();
  };

  /**
   *\english
   *  \brief Base class for the runtime type information of pointer types
   *         except pointer to member
   *\endenglish
   *\german
   *  \brief Basisklasse für die Laufzeittypinformationen von Zeigertypen
   *         außer Zeiger auf Klassenmember
   *\endgerman
   */
  class __pointer_type_info : public __pbase_type_info
  {
    public:
      /**
       *\english
       *  Destructor
       *  \note Needed to generate vtables and type information for this class
       *\endenglish
       *\german
       *  Destruktor
       *  \note Wird benötigt um vtables und Typinformationen für diese Klasse
       *        zu generieren
       *\endgerman
       */
      virtual ~__pointer_type_info();
  };

  /**
   *\english
   *  \brief Base class for the runtime type information of pointer-to-member
   *         types
   *\endenglish
   *\german
   *  \brief Basisklasse für die Laufzeittypinformationen von Zeiger-auf-Member
   *         Typen
   *\endgerman
   */
  class __pointer_to_member_type_info : public __pbase_type_info
  {
    public:
      /**
       *\english
       *  pointer to the type information of the class of the pointer-to-member
       *\endenglish
       *\german
       *  Zeiger auf die Typinformationen der Klasse des Zeigers-auf-Member
       *\endgerman
       */
      const __class_type_info* __context;

      /**
       *\english
       *  Destructor
       *  \note Needed to generate vtables and type information for this class
       *\endenglish
       *\german
       *  Destruktor
       *  \note Wird benötigt um vtables und Typinformationen für diese Klasse
       *        zu generieren
       *\endgerman
       */
      virtual ~__pointer_to_member_type_info();
  };

  /*@}*/

#endif
}



/**
 *\english
 *  \brief Namespace of the C++ ABI
 *
 *  This namespace contains additionally to those members listed here all
 *  members of the __cxxabiv1 namespace.
 *\endenglish
 *\german
 *  \brief Namensraum der C++ ABI
 *
 *  Dieser Namensraum zusätzlich zu den hier gelisteten alle Elemente aus dem
 *  Namensraum __cxxabiv1 .
 *\endgerman
 */
namespace abi
{
  // Lift all classes from __cxxabiv1 to the abi namespace
  using namespace __cxxabiv1;



  /** \addtogroup lightlibcpp_cxxabi_pure_virtual */
  /*@{*/

  extern "C"
  {
    /**
     *\english
     *  This function is called at runtime if a pure virtual function is being
     *  called not overwritten by any derived class.
     *
     *  __cxa_pure_virtual() does not return to the caller.
     *\endenglish
     *\german
     *  Diese Funktion wird zur Laufzeit aufgerufen, falls eine rein-virtuelle
     *  Funktion zum Zeitpunkt des Aufrufs von keiner abgeleiteten Klasse
     *  überschrieben wird.
     *
     *  __cxa_pure_virtual() kehrt nicht zum Aufrufenden zurück.
     *\endgerman
     */
    void __cxa_pure_virtual() _LIGHTLIBCPP_NORETURN;
  }

  /*@}*/
}



#endif
