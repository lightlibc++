default: all

.PHONY: doc todo clean

# The shell used to execute the commands
SHELL:= /bin/bash

# Environment:
ifeq ($(CXX),)
  CXX:= g++
endif
ifeq ($(AR),)
  AR:= ar
endif
ifeq ($(STATIC_LIB),)
  STATIC_LIB:= libc++.a
endif
ifeq ($(SHARED_LIB),)
  SHARED_LIB:= libc++.so
endif
ifneq ($(SHARED_LIB_NAME),)
  SHARED_LIB_NAME:= -Xlinker -h$(SHARED_LIB_NAME)
endif
# CXXFLAGS
ifeq ($(CXXFLAGS_SHARED),)
  CXXFLAGS_SHARED:= -fpic
endif

# Files from the host environment
LIBC:= $(shell $(CXX) -print-file-name=libc.so)
LIBGCC:= $(shell $(CXX) -print-libgcc-file-name)

SRC:= $(shell  [ -d source ] && find source -name "*.cpp")
OBJ:= $(SRC:%.cpp=%.o)
OBJ_SHARED:= $(SRC:%.cpp=%-shared.o)
DEP:= $(SRC:%.cpp=%.dep)
DEP_SHARED:= $(SRC:%.cpp=%-shared.dep)
TESTCASE_SRC:= $(shell [ -d testcase ] && find testcase -name "*.cpp")
TESTCASE:= $(TESTCASE_SRC:%.cpp=%)
TESTCASE_DEP:= $(TESTCASE_SRC:%.cpp=%.dep)
TESTCASE_HOST:= $(TESTCASE_SRC:%.cpp=%-host)
TESTCASE_HOST_DEP:= $(TESTCASE_SRC:%.cpp=%-host.dep)
INCLUDE:= -I include/
CXXFLAGS_COMMON_HOST:= -std=c++0x -Wall -Werror -Wextra -pedantic-errors
CXXFLAGS_COMMON:= $(CXXFLAGS_COMMON_HOST) -fno-exceptions -nostdinc++
CXXFLAGS:= $(CXXFLAGS) $(CXXFLAGS_COMMON) $(INCLUDE)
CXXFLAGS_SHARED:= $(CXXFLAGS_SHARED) $(CXXFLAGS_COMMON) $(INCLUDE)
CXXFLAGS_TESTCASE:= $(CXXFLAGS_COMMON) $(INCLUDE) -nodefaultlibs
LIBS_TESTCASE:= -Xlinker --start-group -Xlinker $(STATIC_LIB) -Xlinker $(LIBGCC) -Xlinker $(LIBC) -Xlinker --end-group

-include $(DEP)
-include $(TESTCASE_DEP) $(TESTCASE_HOST_DEP)

all: $(STATIC_LIB) $(SHARED_LIB)

testcases: $(STATIC_LIB) $(TESTCASE)

testcases-host: $(TESTCASE_HOST)

clean:
	@rm -f $(DEP) $(DEP_SHARED) $(OBJ) $(OBJ_SHARED) $(STATIC_LIB) $(SHARED_LIB) $(TESTCASE_DEP) $(TESTCASE_HOST_DEP) $(TESTCASE) $(TESTCASE_HOST) $(TESTCASE:%=%.gcc) $(TESTCASE:%=%.out) $(TESTCASE_HOST:%=%.gcc) $(TESTCASE_HOST:%=%.out)

doc:
	@rm -rf doc
	@mkdir doc
	@doxygen

todo:
	@grep -r -H -n --exclude=Doxyfile --exclude=Makefile --exclude-dir=doc -e TODO *

kloc:
	@sloccount include source testcase

$(OBJ): Makefile
	@echo " C++  lightlibc++/$(@:%.o=%.cpp)"
	@$(CXX) $(CXXFLAGS) -MMD -MF $(@:%.o=%.dep) -MT $@ -c $(@:%.o=%.cpp) -o $@

$(OBJ_SHARED): Makefile
	@echo " C++  lightlibc++/$(@:%-shared.o=%.cpp)"
	@$(CXX) $(CXXFLAGS_SHARED) -MMD -MF $(@:%.o=%.dep) -MT $@ -c $(@:%-shared.o=%.cpp) -o $@

$(STATIC_LIB): $(OBJ)
	@echo " AR   $@"
	@$(AR) rc $(STATIC_LIB) $(OBJ)

$(SHARED_LIB): $(OBJ_SHARED)
	@echo " C++  $@"
	@$(CXX) -nostdlib -shared $(OBJ_SHARED) -o $@ $(SHARED_LIB_NAME) $(LIBGCC) $(LIBC)

$(TESTCASE): $(STATIC_LIB) Makefile
	@( $(CXX) $(CXXFLAGS_TESTCASE) -MMD -MF $(@:%=%.dep) -MT $@ $(@:%=%.cpp) $(LIBS_TESTCASE) -o $@ 2> $(@:%=%.gcc) && ./testcase.sh $@ $@ ) || echo -e "\033[01;31mfailed to compile\033[m: $@"

$(TESTCASE_HOST): Makefile
	@( $(CXX) $(CXXFLAGS_COMMON_HOST) -MMD -MF $(@:%=%.dep) -MT $@ $(@:%-host=%.cpp) -o $@ 2> $(@:%=%.gcc) && ./testcase.sh $@ $(@:%-host=%) ) || echo -e "\033[01;31mfailed to compile\033[m: $(@:%-host=%)"
