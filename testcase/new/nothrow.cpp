/*
Permission is granted to use, modify, and / or redistribute at will.

This includes removing authorship notices, re-use of code parts in
other software (with or without giving credit), and / or creating a
commercial product based on it.

This permission is not revocable by the author.

This software is provided as-is. Use it at your own risk. There is
no warranty whatsoever, neither expressed nor implied, and by using
this software you accept that the author(s) shall not be held liable
for any loss of data, loss of service, or other damages, be they
incidental or consequential. Your only option other than accepting
this is not to use the software at all.
*/
#include <new>
#include <cstdlib>
#include <limits>            // std::numeric_limits<std::size_t>



/*
 * Checks that the nothrow-new does not throw, but return 0, when it is faced
 * with an unsatisfiable request
 */



int main()
{
  if (new (std::nothrow) char[std::numeric_limits<std::size_t>::max()] == 0)
    return EXIT_SUCCESS;

  return EXIT_FAILURE;
}
