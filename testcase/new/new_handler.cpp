/*
Permission is granted to use, modify, and / or redistribute at will.

This includes removing authorship notices, re-use of code parts in
other software (with or without giving credit), and / or creating a
commercial product based on it.

This permission is not revocable by the author.

This software is provided as-is. Use it at your own risk. There is
no warranty whatsoever, neither expressed nor implied, and by using
this software you accept that the author(s) shall not be held liable
for any loss of data, loss of service, or other damages, be they
incidental or consequential. Your only option other than accepting
this is not to use the software at all.
*/
#include <new>
#include <cstdlib>
#include <limits>            // std::numeric_limits<std::size_t>



/*
 * Checks that the new_handler is 0 on startup and that new calls an installed
 * new_handler when it is faced with an unsatisfiable request
 */



void my_new_handler()
{
  std::exit(EXIT_SUCCESS);
}

int main()
{
  if (std::set_new_handler(my_new_handler) != 0)
    return EXIT_FAILURE;

  new char[std::numeric_limits<std::size_t>::max()];
  return EXIT_FAILURE;
}
