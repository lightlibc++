/*
Permission is granted to use, modify, and / or redistribute at will.

This includes removing authorship notices, re-use of code parts in
other software (with or without giving credit), and / or creating a
commercial product based on it.

This permission is not revocable by the author.

This software is provided as-is. Use it at your own risk. There is
no warranty whatsoever, neither expressed nor implied, and by using
this software you accept that the author(s) shall not be held liable
for any loss of data, loss of service, or other damages, be they
incidental or consequential. Your only option other than accepting
this is not to use the software at all.
*/
#include <cstdlib>
#include <cassert>



/*
 * Check that the quick_exit functions get called in the right order
 */



#ifndef _LIGHTLIBCPP_CPP03_STRICT

  static int count = 0;

  void e()
  {
    assert(count == 2);
    std::_Exit(EXIT_SUCCESS);
  }

  void f()
  {
    assert(count == 1);
    count = 2;
  }

  void g()
  {
    count = 1;
    assert(std::at_quick_exit(f) == 0);
  }

  int main()
  {
    assert(std::at_quick_exit(e) == 0);
    assert(std::at_quick_exit(g) == 0);
    std::quick_exit(EXIT_FAILURE);

    return EXIT_FAILURE;
  }

#else

  int main()
  {
    return EXIT_SUCCESS;
  }

#endif
