/*
Permission is granted to use, modify, and / or redistribute at will.

This includes removing authorship notices, re-use of code parts in
other software (with or without giving credit), and / or creating a
commercial product based on it.

This permission is not revocable by the author.

This software is provided as-is. Use it at your own risk. There is
no warranty whatsoever, neither expressed nor implied, and by using
this software you accept that the author(s) shall not be held liable
for any loss of data, loss of service, or other damages, be they
incidental or consequential. Your only option other than accepting
this is not to use the software at all.
*/
#include <cstdlib>
#include <exception>
#include <cassert>



/*
 * Check that the quick_exit function calls std::terminate when a handler
 * leaks an exception
 */



#if !defined(_LIGHTLIBCPP_CPP03_STRICT) && \
    !defined(_LIGHTLIBCPP_NO_EXCEPTIONS)

  void f()
  {
    throw std::exception();
  }

  int main()
  {
    assert(std::at_quick_exit(f) == 0);
    std::quick_exit(EXIT_SUCCESS);

    return EXIT_FAILURE;
  }

#else

  int main()
  {
    return EXIT_SUCCESS;
  }

#endif
