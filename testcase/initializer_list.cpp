/*
Permission is granted to use, modify, and / or redistribute at will.

This includes removing authorship notices, re-use of code parts in
other software (with or without giving credit), and / or creating a
commercial product based on it.

This permission is not revocable by the author.

This software is provided as-is. Use it at your own risk. There is
no warranty whatsoever, neither expressed nor implied, and by using
this software you accept that the author(s) shall not be held liable
for any loss of data, loss of service, or other damages, be they
incidental or consequential. Your only option other than accepting
this is not to use the software at all.
*/
#include <cstdlib>
#include <cassert>



/*
 * Check that the initializer_list class works
 */



#if defined(_LIGHTLIBCPP_CPP10) && \
    !defined(_LIGHTLIBCPP_CPP10_NO_INITIALIZER_LISTS)

  #include <initializer_list>



  template<typename T>
  class container
  {
    public:
      container(const std::initializer_list<T>& sequence)
      {
        assert(sequence.size() == 3);
        assert(*(sequence.begin() + 0) == 1);
        assert(*(sequence.begin() + 1) == 2);
        assert(*(sequence.begin() + 2) == 3);
        assert(sequence.begin() + sequence.size() == sequence.end());
      }
  };

  int main()
  {
    container<int> cont{1, 2, 3};
    return EXIT_SUCCESS;
  }

#else

  int main()
  {
    return EXIT_SUCCESS;
  }

#endif
