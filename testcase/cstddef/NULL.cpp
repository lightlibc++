/*
Permission is granted to use, modify, and / or redistribute at will.

This includes removing authorship notices, re-use of code parts in
other software (with or without giving credit), and / or creating a
commercial product based on it.

This permission is not revocable by the author.

This software is provided as-is. Use it at your own risk. There is
no warranty whatsoever, neither expressed nor implied, and by using
this software you accept that the author(s) shall not be held liable
for any loss of data, loss of service, or other damages, be they
incidental or consequential. Your only option other than accepting
this is not to use the software at all.
*/
#include <cstddef>
#include <cstdlib>
#include <typeinfo>



/*
 * Checks that the type of NULL is not void*. If this test fails your libc is
 * at fault and should be fixed. You have to define NULL to an
 * "implementation-defined C++ null pointer constant". "Possible definitions
 * include 0 and OL, but not (void*)0" (Kapitel 18.1 ISO/IEC 14882:2003).
 */



int main()
{
#ifndef _LIGHTLIBCPP_NO_RTTI
  if (typeid(NULL) == typeid(void*))
    return EXIT_FAILURE;
#endif

  return EXIT_SUCCESS;
}
