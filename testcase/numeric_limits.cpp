/*
Permission is granted to use, modify, and / or redistribute at will.

This includes removing authorship notices, re-use of code parts in
other software (with or without giving credit), and / or creating a
commercial product based on it.

This permission is not revocable by the author.

This software is provided as-is. Use it at your own risk. There is
no warranty whatsoever, neither expressed nor implied, and by using
this software you accept that the author(s) shall not be held liable
for any loss of data, loss of service, or other damages, be they
incidental or consequential. Your only option other than accepting
this is not to use the software at all.
*/
#include <cstdlib>
#include <cassert>
#include <limits>
#include <climits>



/*
 * Check that the initializer_list class works
 */



int main()
{
  assert(std::numeric_limits<bool>::is_specialized == true);
  assert(std::numeric_limits<bool>::min_exponent == 0);
  assert(std::numeric_limits<int>::max() == INT_MAX);
  return EXIT_SUCCESS;
}
