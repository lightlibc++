/*
Permission is granted to use, modify, and / or redistribute at will.

This includes removing authorship notices, re-use of code parts in
other software (with or without giving credit), and / or creating a
commercial product based on it.

This permission is not revocable by the author.

This software is provided as-is. Use it at your own risk. There is
no warranty whatsoever, neither expressed nor implied, and by using
this software you accept that the author(s) shall not be held liable
for any loss of data, loss of service, or other damages, be they
incidental or consequential. Your only option other than accepting
this is not to use the software at all.
*/
#include <typeinfo>
#include <cstdlib>
#include <stdio.h>



/*
 * Checks that type information is properly generated
 */



#ifndef _LIGHTLIBCPP_NO_RTTI

  enum some_enum
  {
  };

  class some_class
  {
    public:
      void f(){}
      virtual ~some_class(){}
  };

  class single_derived : public some_class
  {
  };

  class some_other_base
  {
    public:
      virtual ~some_other_base(){}
  };

  class multiple_derived : public some_class,
			  public some_other_base
  {
  };

  int main()
  {
    /* Check type information for the fundamental types */
    printf("%s\n", typeid(void).name());
    printf("%s\n", typeid(bool).name());
    printf("%s\n", typeid(wchar_t).name());
    printf("%s\n", typeid(char).name());
    printf("%s\n", typeid(unsigned char).name());
    printf("%s\n", typeid(signed char).name());
    printf("%s\n", typeid(short).name());
    printf("%s\n", typeid(unsigned short).name());
    printf("%s\n", typeid(int).name());
    printf("%s\n", typeid(unsigned int).name());
    printf("%s\n", typeid(long).name());
    printf("%s\n", typeid(unsigned long).name());
  #ifndef _LIGHTLIBCPP_NO_LONGLONG
    printf("%s\n", typeid(long long).name());
    printf("%s\n", typeid(unsigned long long).name());
  #endif
    printf("%s\n", typeid(float).name());
    printf("%s\n", typeid(double).name());
    printf("%s\n", typeid(long double).name());

    /* Check type information for array types */
    printf("%s\n", typeid(int [5]).name());

    /* Check type information for function types */
    printf("%s\n", typeid(main).name());

    /* Check type information for enum types */
    printf("%s\n", typeid(some_enum).name());

    /* Check type information for normal class */
    printf("%s\n", typeid(some_class).name());

    /* Check type information for class with single inheritance */
    printf("%s\n", typeid(single_derived).name());

    /* Check type information for class with multiple inheritance */
    printf("%s\n", typeid(multiple_derived).name());

    /* Check type information for fundamental pointer types */
    printf("%s\n", typeid(void*).name());
    printf("%s\n", typeid(bool*).name());
    printf("%s\n", typeid(wchar_t*).name());
    printf("%s\n", typeid(char*).name());
    printf("%s\n", typeid(unsigned char*).name());
    printf("%s\n", typeid(signed char*).name());
    printf("%s\n", typeid(short*).name());
    printf("%s\n", typeid(unsigned short*).name());
    printf("%s\n", typeid(int*).name());
    printf("%s\n", typeid(unsigned int*).name());
    printf("%s\n", typeid(long*).name());
    printf("%s\n", typeid(unsigned long*).name());
  #ifndef _LIGHTLIBCPP_NO_LONGLONG
    printf("%s\n", typeid(long long*).name());
    printf("%s\n", typeid(unsigned long long*).name());
  #endif
    printf("%s\n", typeid(float*).name());
    printf("%s\n", typeid(double*).name());
    printf("%s\n", typeid(long double*).name());
    printf("%s\n", typeid(const void*).name());
    printf("%s\n", typeid(const bool*).name());
    printf("%s\n", typeid(const wchar_t*).name());
    printf("%s\n", typeid(const char*).name());
    printf("%s\n", typeid(const unsigned char*).name());
    printf("%s\n", typeid(const signed char*).name());
    printf("%s\n", typeid(const short*).name());
    printf("%s\n", typeid(const unsigned short*).name());
    printf("%s\n", typeid(const int*).name());
    printf("%s\n", typeid(const unsigned int*).name());
    printf("%s\n", typeid(const long*).name());
    printf("%s\n", typeid(const unsigned long*).name());
  #ifndef _LIGHTLIBCPP_NO_LONGLONG
    printf("%s\n", typeid(const long long*).name());
    printf("%s\n", typeid(const unsigned long long*).name());
  #endif
    printf("%s\n", typeid(const float*).name());
    printf("%s\n", typeid(const double*).name());
    printf("%s\n", typeid(const long double*).name());

    /* Check type information for pointer to member types */
    printf("%s\n", typeid(&some_class::f).name());

    return EXIT_SUCCESS;
  }

#else

  int main()
  {
    return EXIT_SUCCESS;
  }

#endif
