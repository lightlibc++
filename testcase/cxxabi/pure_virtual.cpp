/*
Permission is granted to use, modify, and / or redistribute at will.

This includes removing authorship notices, re-use of code parts in
other software (with or without giving credit), and / or creating a
commercial product based on it.

This permission is not revocable by the author.

This software is provided as-is. Use it at your own risk. There is
no warranty whatsoever, neither expressed nor implied, and by using
this software you accept that the author(s) shall not be held liable
for any loss of data, loss of service, or other damages, be they
incidental or consequential. Your only option other than accepting
this is not to use the software at all.
*/
#include <cstdlib>



/*
 * NOTE: This testcase is supposed to fail during execution
 * This testcase checks that a handler for pure virtual functions that are
 * not properly set is called
 */



class base
{
  public:
    base()
    {
      // f1 calls our pure virtual function,
      // but derived is not yet constructed
      f1();
    }

    void f1()
    {
      f2();
    }

    virtual void f2() = 0;
    virtual ~base(){}
};

class derived : public base
{
  public:
    virtual void f2()
    {
    }
};

int main()
{
  derived* d = new derived();
  d->f2();
  return EXIT_SUCCESS;
}
