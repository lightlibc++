#! /bin/bash
# $1 executable path
# $2 user visible name

./$1

if [ $? == 0 ]
then
  exit 1
fi

exit 0
