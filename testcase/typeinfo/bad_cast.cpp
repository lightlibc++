/*
Permission is granted to use, modify, and / or redistribute at will.

This includes removing authorship notices, re-use of code parts in
other software (with or without giving credit), and / or creating a
commercial product based on it.

This permission is not revocable by the author.

This software is provided as-is. Use it at your own risk. There is
no warranty whatsoever, neither expressed nor implied, and by using
this software you accept that the author(s) shall not be held liable
for any loss of data, loss of service, or other damages, be they
incidental or consequential. Your only option other than accepting
this is not to use the software at all.
*/
#include <typeinfo>
#include <cstdlib>
// TODO #include <cstdio>
#include <stdio.h>



/*
 * Trigger a std::bad_cast exception
 */



#ifndef _LIGHTLIBCPP_NO_EXCEPTIONS

  class some_class
  {
    public:
      virtual ~some_class(){}
  };

  class some_derived : public some_class
  {
  };

  int main()
  {
    some_class* p = new some_class();

    try
    {
      dynamic_cast<some_derived&>(*p);
    }
    catch (std::bad_cast& e)
    {
      delete p;
      return EXIT_SUCCESS;
    }

    delete p;
    return EXIT_FAILURE;
  }

#else

  int main()
  {
    return EXIT_SUCCESS;
  }

#endif
