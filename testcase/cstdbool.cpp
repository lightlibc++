/*
Permission is granted to use, modify, and / or redistribute at will.

This includes removing authorship notices, re-use of code parts in
other software (with or without giving credit), and / or creating a
commercial product based on it.

This permission is not revocable by the author.

This software is provided as-is. Use it at your own risk. There is
no warranty whatsoever, neither expressed nor implied, and by using
this software you accept that the author(s) shall not be held liable
for any loss of data, loss of service, or other damages, be they
incidental or consequential. Your only option other than accepting
this is not to use the software at all.
*/
#include <cstdlib>



/*
 * Check that including cstdbool compiles and that
 * __bool_true_false_are_defined is defined to 1
 */



#ifndef _LIGHTLIBCPP_CPP03_STRICT

  #include <cstdbool>



  int main()
  {
  #if __bool_true_false_are_defined == 1
    return EXIT_SUCCESS;
  #else
    return EXIT_FAILURE;
  #endif
  }

#else

  int main()
  {
    return EXIT_SUCCESS;
  }

#endif
