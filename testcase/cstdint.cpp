/*
Permission is granted to use, modify, and / or redistribute at will.

This includes removing authorship notices, re-use of code parts in
other software (with or without giving credit), and / or creating a
commercial product based on it.

This permission is not revocable by the author.

This software is provided as-is. Use it at your own risk. There is
no warranty whatsoever, neither expressed nor implied, and by using
this software you accept that the author(s) shall not be held liable
for any loss of data, loss of service, or other damages, be they
incidental or consequential. Your only option other than accepting
this is not to use the software at all.
*/
#include <cstdlib>

#ifndef _LIGHTLIBCPP_CPP03_STRICT

  #define __STDC_LIMIT_MACROS            1
  #define __STDC_CONSTANT_MACROS         1
  #include <typeinfo>
  #include <cstdint>
  #include <cassert>
  // TODO: #include <climits> // CHAR_BIT
  #include <limits.h>

  /*
  * Checks whether <cstdint> complies to the C Standard
  */

  template<typename T,
	  T base,
	  unsigned int exponent>
  struct power
  {
    static const T value = base * power<T, base, exponent - 1>::value;
  };

  template<typename T,
	  T base>
  struct power<T, base, 0>
  {
    static const T value = 1;
  };

  template<unsigned int exponent>
  struct signed_power_of_2
  {
    static const std::intmax_t value = power<std::intmax_t, 2, exponent>::value;
  };

  template<unsigned int exponent>
  struct unsigned_power_of_2
  {
    static const std::uintmax_t value = power<std::uintmax_t, 2, exponent>::value;
  };

  #define ASSERT_TYPE_OF_SIZE(x)       assert(sizeof(std::int##x##_t) * CHAR_BIT == x); \
                                       assert(sizeof(std::uint##x##_t) * CHAR_BIT == x); \
                                       assert(sizeof(std::int_least##x##_t) * CHAR_BIT >= x); \
                                       assert(sizeof(std::uint_least##x##_t) * CHAR_BIT >= x); \
                                       assert(sizeof(std::int_fast##x##_t) * CHAR_BIT >= x); \
                                       assert(sizeof(std::uint_fast##x##_t) * CHAR_BIT >= x);
  #define ASSERT_SIZE_MACRO(x)         assert(INT##x##_MIN == - signed_power_of_2<x - 1>::value); \
                                       assert(INT##x##_MAX == signed_power_of_2<x - 1>::value - 1); \
                                       assert(UINT##x##_MAX == unsigned_power_of_2<x>::value - 1); \
                                       assert(INT_LEAST##x##_MIN <= - (signed_power_of_2<x - 1>::value - 1)); \
                                       assert(INT_LEAST##x##_MAX >= signed_power_of_2<x - 1>::value - 1); \
                                       assert(UINT_LEAST##x##_MAX >= unsigned_power_of_2<x>::value - 1); \
                                       assert(INT_FAST##x##_MIN <= - (signed_power_of_2<x - 1>::value - 1)); \
                                       assert(INT_FAST##x##_MAX >= signed_power_of_2<x - 1>::value - 1); \
                                       assert(UINT_FAST##x##_MAX >= unsigned_power_of_2<x>::value - 1);

  int i;

  int main()
  {
    /*
    * assertions for the sizes of the new types
    */
    ASSERT_TYPE_OF_SIZE(8);
    ASSERT_TYPE_OF_SIZE(16);
    ASSERT_TYPE_OF_SIZE(32);
    ASSERT_TYPE_OF_SIZE(64);

    void* p = &i;
    assert(reinterpret_cast<void*>(reinterpret_cast<std::intptr_t>(p)) == p);
    assert(reinterpret_cast<void*>(reinterpret_cast<std::uintptr_t>(p)) == p);



    /*
    * assertions for the type size macros
    */
    ASSERT_SIZE_MACRO(8);
    ASSERT_SIZE_MACRO(16);
    ASSERT_SIZE_MACRO(32);
  #ifndef _LIGHTLIBCPP_NO_LONGLONG
    assert(INT64_MIN == static_cast<signed long long>(-0x8000000000000000LL));
    assert(INT64_MAX == 0x7FFFFFFFFFFFFFFFLL);
    assert(UINT64_MAX == 0xFFFFFFFFFFFFFFFFULL);
    assert(INT_LEAST64_MIN <= -0x7FFFFFFFFFFFFFFFLL);
    assert(INT_LEAST64_MAX >= 0x7FFFFFFFFFFFFFFFLL);
    assert(UINT_LEAST64_MAX >= 0xFFFFFFFFFFFFFFFFULL);
    assert(INT_FAST64_MIN <= -0x7FFFFFFFFFFFFFFFLL);
    assert(INT_FAST64_MAX >= 0x7FFFFFFFFFFFFFFFLL);
    assert(UINT_FAST64_MAX >= 0xFFFFFFFFFFFFFFFFULL);
  #endif

    assert(INTPTR_MIN <= - (signed_power_of_2<15>::value - 1));
    assert(INTPTR_MAX >= unsigned_power_of_2<15>::value - 1);
    assert(UINTPTR_MAX >= unsigned_power_of_2<16>::value - 1);

  #ifndef _LIGHTLIBCPP_NO_LONGLONG
    assert(INTMAX_MIN <= -0x7FFFFFFFFFFFFFFFLL);
    assert(INTMAX_MAX >= 0x7FFFFFFFFFFFFFFFLL);
    assert(UINTMAX_MAX >= 0xFFFFFFFFFFFFFFFFULL);
  #endif

    // TODO: testcases for macros {PTRDIFF, SIG_ATOMIC, WCHAR, WINT}_{MIN, MAX} SIZE_MAX



    /*
    * assertions for the type of the macros for integer constants
    */

  #if defined(_LIGHTLIBCPP_CPP10) && !defined(_LIGHTLIBCPP_NO_RTTI)
    assert(typeid(INTMAX_C(0)) == typeid(std::intmax_t));
    assert(typeid(UINTMAX_C(0)) == typeid(std::uintmax_t));
  #endif

    return EXIT_SUCCESS;
  }

#else

  int main()
  {
    return EXIT_SUCCESS;
  }

#endif
