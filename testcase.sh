#! /bin/bash
# $1 executable path
# $2 user visible name

if [ -x $2.sh ]
then
  ./$2.sh $1 $2 > $1.out 2>&1
else
  ./$1 > $1.out 2>&1
fi

if [ $? == 0 ]
then
  echo -e "\033[01;32mpassed\033[m: $2"
else
  echo -e "\033[01;31mfailed\033[m: $2"
  rm -f $1
fi

exit 0
